---
description: >-
  There is no optimal way to play the game; Use the resources here to understand
  the game better , do your own further research, and determine what is best for
  you!
---

# Core Gameplay Loop

Tales of Elleria - Prologue

Elleria is structured to be released in different phases, each with different mechanics and purposes. In the prologue, users are getting started and ask questions such as:

> What is Elleria; what are Heroes; how do we get stronger; what do we do?

Elleria's prologue is structured around an **idle NFT staking & turn-based battle system**. This design allows users to ease into the world with relatively low effort. These simple mechanics are essential to the success of the prologue, allowing players without time to spare to still participate.

* Players can easily stake NFTs for $MEDALS emissions, used in other game upgrades and transactions.
* Players can easily and quickly embark on quests.
* The core gameplay loop is simple - mint, stake, fight.

Take heed! The system may seem easy, but it has hidden depth to reward those who truly researched and prepared.

{% content-ref url="player-types.md" %}
[player-types.md](player-types.md)
{% endcontent-ref %}

## Core Gameplay Loop

<figure><img src="../../.gitbook/assets/image (1) (1) (3).png" alt=""><figcaption><p>Core Gameplay Loop for ToE- Red Arrows = Inflationary; Green = Deflationary</p></figcaption></figure>

**Succinct**

1. Summon/Buy Heroes
2. Send Idle Heroes on Assignments to earn $MEDALS
3. Use $MEDALS to strengthen your heroes and go on quests to earn $ELM
4. Use $ELM to summon and further strengthen your heroes to go on more dangerous quests with higher rewards.

#### Narrative

Outside Elleria, monsters run rampant and require the active participation of heroes to keep the city safe. Teams of Heroes emerge and embark on dangerous quests. If successful, they reap tremendous rewards and are honoured as legends forever.

As a citizen of Elleria, Elysis has granted you the power to summon heroes. Summon heroes and build a team that you can use on your adventures!

Heroes can take on assignments for $MEDALS, with different requirements and emission rates. Use $MEDALS in game features such as strengthening heroes, questing, blacksmithing/enchanting.

Go on quests to defeat monsters for rewards, equipment, and $ELM! Quests vary in difficulty and require expertise to reign victorious. $ELM is needed to craft equipment and summon or strengthen heroes.

The final goal of TELL would be to gather an elite team and work together with other players to slay Ignacio, bringing peace to Elleria.

Ignacio's den lies deep within uncharted territories, guarded by monsters and harsh terrain. To reach it, players have to do enough quests to map out the surrounding area to find a way to get to the dragon.

Everyone's actions in-game affect the development of the world.\
\
**Let's explore the world together!**

{% hint style="warning" %}
**Note:** Summoned Heroes, $ELM, and $MEDALS must be bridged into the game to be used. Please check [heroes-relics-elm-bridging.md](heroes-relics-elm-bridging.md "mention") for more information.
{% endhint %}
