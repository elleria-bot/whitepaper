# Equipment System

Equipment are items in Tales of Elleria that can be equipped on heroes to strengthen their combat stats. There are currently 4 types of Equipment:&#x20;

* Charms
* Circlets
* Armguards
* Skins

Only one of each type can be equipped on a hero at a time.

With the exception of skins, equipment have 1 **main stat and 2 sub-stats/secondary stats. These stats allow equipment to** be imbued with powerful properties and attributes and can greatly enhance a hero's combat capabilities.

***

## Equipment Gameplay Loop

<figure><img src="../../.gitbook/assets/image (13).png" alt=""><figcaption></figcaption></figure>

The equipment system has been designed to be accessible but also offers depth for seasoned players. Designed to be intuitive, newcomers should be able to grasp the basics swiftly without being overwhelmed!

The simplified equipment gameplay loop is as such:

1. Craft Equipment with Equipment Mold + Relics + Magic Shards. Crafted equipment will randomly roll their main and secondary stats.
2. (Optional) Upgrade equipment with $MEDALS and $ELM to receive bonus stats.
3. (Optional) Reroll equipment to get ideal secondary stats.
4. (Optional) Infuse gems and metals into equipment to get extra combat buffs.
5. Equip multiple pieces of a set to receive a bonus set effect.

***

## Obtaining Equipment

Equipment can be obtained from 'Equipment Crafting' or from the [marketplace](https://trove.treasure.lol/collection/tales-of-elleria-equipment).

<figure><img src="../../.gitbook/assets/image (1) (1).png" alt=""><figcaption></figcaption></figure>

Players can craft Equipment from Heroes Quarters > Manage Equipment. All information regarding crafting requirements and outcomes can be seen from the in-game UI.

{% hint style="warning" %}
Please note that the main and sub-stat roll chances are uneven!\
Refer to the table below for the main and sub-stat roll chances.
{% endhint %}

### Main Stat Roll Probabilities

{% tabs %}
{% tab title="Charms" %}
| Stats           | Chance |
| --------------- | ------ |
| Magical Attack  | 50%    |
| Physical Attack | 50%    |
{% endtab %}

{% tab title="Armguards" %}
| Stats | Chance |
| ----- | ------ |
| HP    | 100%   |
{% endtab %}

{% tab title="Circlets" %}
| Stats           | Chance |
| --------------- | ------ |
| HP              | 20%    |
| HP %            | 7%     |
| DEF %           | 7%     |
| RES %           | 7%     |
| Physical Attack | 20%    |
| Magcial Attack  | 20%    |
| Final Attack %  | 5%     |
| Skill Damage %  | 14%    |
{% endtab %}
{% endtabs %}

### Sub Stat Roll Probabilities

{% tabs %}
{% tab title="Charms" %}
| Stats           | Chance |
| --------------- | ------ |
| DEF             | 17%    |
| DEF %           | 5.00%  |
| RES             | 17%    |
| RES %           | 5.00%  |
| HP              | 17%    |
| HP %            | 5.00%  |
| Magical Attack  | 17%    |
| Physical Attack | 17%    |
{% endtab %}

{% tab title="Armguards" %}
| Stats          | Chance |
| -------------- | ------ |
| DEF            | 18%    |
| DEF %          | 7%     |
| RES            | 18%    |
| RES %          | 7%     |
| HP             | 18%    |
| HP %           | 7%     |
| AGI            | 18%    |
| Skill Damage % | 7%     |
{% endtab %}

{% tab title="Circlets" %}
| Stats           | Chance |
| --------------- | ------ |
| DEF %           | 7%     |
| RES %           | 7%     |
| HP              | 15%    |
| HP %            | 7%     |
| Magical Attack  | 15%    |
| Physical Attack | 15%    |
| Final Attack %  | 6%     |
| Skill Damage %  | 10%    |
| AGI             | 18%    |
{% endtab %}
{% endtabs %}

## Equipping

<figure><img src="../../.gitbook/assets/image (3) (1).png" alt=""><figcaption></figcaption></figure>

Players can equip their obtained Equipment from the Hero information page, accessible from Heroes Quarters > Manage Heroes > Idle.&#x20;

Click on the boxes beside the shadow avatar to view equipable pieces!\
(For skins, click the hanger button beside the pfp)

{% hint style="info" %}
Players can only manage equipment for IDLE heroes (Binded and not on a quest nor in assignments). If the hero is still on-chain, please bridge it via the Summoning Altar.
{% endhint %}

## Enhancing

Equipment can be enhanced using $MEDALS, $ELM, and Gems/Metals. There are 3 different ways to enhance a piece: Upgrade, Reroll, and Infusion.

### Upgrade

<figure><img src="../../.gitbook/assets/image (5) (1).png" alt=""><figcaption></figcaption></figure>

An Equipment's main stat can be upgraded up to 15 ★. The stat improvement along with its upgrade cost will increase non-linearly every 5 ★. The upgrade effect is fixed based on the main stat and tier.

From 8 ★ onwards, there is a chance for the upgrade level to decrease on failure. Players can use extra resources to protect against the decrease.

<table><thead><tr><th width="140">★ Level</th><th width="130">Success %</th><th width="109">Fail %</th><th width="126">Decrease %</th><th>Upgrade Weight %</th></tr></thead><tbody><tr><td>0 > 1</td><td>100%</td><td>0%</td><td>0%</td><td>2.67%</td></tr><tr><td>1 > 2</td><td>100%</td><td>0%</td><td>0%</td><td>2.67%</td></tr><tr><td>2 > 3</td><td>100%</td><td>0%</td><td>0%</td><td>2.67%</td></tr><tr><td>3 > 4</td><td>100%</td><td>0%</td><td>0%</td><td>2.67%</td></tr><tr><td>4 > 5</td><td>100%</td><td>0%</td><td>0%</td><td>2.67%</td></tr><tr><td>5 > 6</td><td>90%</td><td>10%</td><td>0%</td><td>4.00%</td></tr><tr><td>6 > 7</td><td>80%</td><td>20%</td><td>0%</td><td>4.00%</td></tr><tr><td>7 > 8</td><td>70%</td><td>30%</td><td>0%</td><td>4.00%</td></tr><tr><td>8 > 9</td><td>60%</td><td>30%</td><td>10%</td><td>4.00%</td></tr><tr><td>9 > 10</td><td>50%</td><td>30%</td><td>20%</td><td>4.00%</td></tr><tr><td>10 > 11</td><td>60%</td><td>40%</td><td>0%</td><td>13.33%</td></tr><tr><td>11 > 12</td><td>30%</td><td>40%</td><td>30%</td><td>13.33%</td></tr><tr><td>12 > 13</td><td>20%</td><td>40%</td><td>40%</td><td>13.33%</td></tr><tr><td>13 > 14</td><td>15%</td><td>40%</td><td>45%</td><td>13.33%</td></tr><tr><td>14 > 15</td><td>10%</td><td>40%</td><td>50%</td><td>13.33%</td></tr></tbody></table>

{% hint style="info" %}
There is no level reduction rate from 10★ > 11★!
{% endhint %}

View a table of all upgrade costs [here](upgrade-costs.md).

### Reroll

<figure><img src="../../.gitbook/assets/image (4) (1).png" alt=""><figcaption></figcaption></figure>

An equipment's sub-stats can be rerolled using $ELM to achieve the ideal combination without having to craft a new piece. A detailed list of achievable sub-stats can be found [here](stat-tables.md#sub-stats). The cost of the re-roll is based on its tier.

| Equipment Tier | $ELM Cost |
| -------------- | --------- |
| 1              | 5 $ELM    |
| 2              | 10 $ELM   |
| 3              | 20 $ELM   |

{% hint style="warning" %}
Please note that the sub-stat roll chances are uneven! \
For the full list of sub-stat chances, please refer [here](./#sub-stat-roll-probabilities)!&#x20;
{% endhint %}

### Infusion

<figure><img src="../../.gitbook/assets/image (24).png" alt=""><figcaption></figcaption></figure>

Some Relics (Gems and Metals) can be permanently infused into a piece of equipment to grant bonus combat buffs. Different effects can occur based on the infused relic's type, tier, and equipment category.

Up to 2 relics can be infused at once. Infused relics can be replaced with new ones. Find a list of possible infusions [here](infusion-effects.md).

{% hint style="warning" %}
Warning: Infused Relics can never be removed. When applying a new infusion over an existing one, the existing relic will be burnt permanently.
{% endhint %}

## Equipment Type

Equipment is classified into the following categories and types:

{% tabs %}
{% tab title="Accessories" %}
Versatile pieces of equipment that offer a wide range of stat improvements. Accessories are crucial in tailoring a hero's stats to suit his skill set!

* **Charms**
* **???**
* **???**
{% endtab %}

{% tab title="Armor" %}
Armor focuses primarily on defensive stats, contributing to your hero's overall survivability. Indispensable for those who prefer tanky playstyles.

* Circlets
* Armguards
* ???
{% endtab %}

{% tab title="Weapon" %}
Weapons are designed exclusively to enhance a hero's offensive capabilities, amplifying damage output in various ways.

* ???
* ???
{% endtab %}
{% endtabs %}

## Set Effects

Some Equipment sets have additional effects as follows:

Tier 2:

{% tabs %}
{% tab title="Wonderer's Wear" %}
\[3 pieces]: +2% M. ATK
{% endtab %}

{% tab title="Adventurer's Armor" %}
\[3 pieces]: +2% P. ATK
{% endtab %}
{% endtabs %}

Tier 3:

{% tabs %}
{% tab title="Ooze's Oracle" %}
\[2 pieces]: +5% RES\
\[3 pieces]: +3% Combat INT\
\[3 pieces]: +5% Crit Rate
{% endtab %}

{% tab title="Gremlin's Guise" %}
\[2 pieces]: +5% DEF\
\[3 pieces]: +3% Combat STR\
\[3 pieces]: +5% Crit Rate
{% endtab %}
{% endtabs %}

{% hint style="warning" %}
Piece requirements may change in the future as more types get added.
{% endhint %}

***

## Stats

With the exception of skins, each piece of equipment has one main stat, determined by the piece's type and category when crafted. There is a range for the main stat's value that cannot be changed after crafting.

Different types have their own sets of possible main stats and sub-stats.&#x20;

{% tabs %}
{% tab title="Charms" %}
Possible Main Stats

* Physical Attack
* Magic Attack

Possible Sub Stats

* Defense / Defense %
* Resistance / Resistance %
* HP / HP %
* Physical Attack
* Magic Attack
{% endtab %}

{% tab title="Armguards" %}
Possible Main Stats

* HP

Possible Sub Stats

* HP / HP %
* Defense / Defense %
* Resistance / Resistance %
* Agility
* Skill Damage %
{% endtab %}

{% tab title="Circlets" %}
Possible Main Stats

* HP / HP %
* Defense %
* Resistance %
* Physical Attack
* Magic Attack
* Final Damage %
* Skill Damage %

Possible Sub Stats

* HP / HP %
* Defense %
* Resistance %
* Physical Attack
* Magic Attack
* Final Damage %
* Skill Damage %
* Combat Agility
{% endtab %}
{% endtabs %}

A list of detailed ranges is available [here](stat-tables.md).

## **Transfer Limits**

**To prevent abuse, equipped pieces belonging to heroes that have quested cannot be unequipped until the next quest cycle.**
