# Infusion Effects

## Material Sockets

Some pieces of equipment have [sockets ](./#infusion)that will allow players to infuse relics (gems/metals) to gain additional combat boosts. These boosts are fixed and only vary by relic tier and type.

Infusion is a guaranteed way to increase the stats you require. However, note that players can replace infused relics with new ones, but infused relics can never be removed.

## Obtaining Gem/Metals

There are a total of 6 gems and 5 metals, each providing different effects. 5x of each gem can be merged up to T6, while 10x of each metal can be merged up to T5.

Players can merge these materials through the [relic crafting system](../crafting-system/#how-to-craft).

## Infusion Effects

### Accessories (Charms)

<table><thead><tr><th width="140">Relic Type</th><th width="167">Infusion Effect</th><th width="72">T1</th><th width="80">T2</th><th width="71">T3</th><th width="76">T4</th><th width="85">T5</th><th>T6</th></tr></thead><tbody><tr><td>Ruby</td><td>+Combat STR</td><td>1</td><td>2</td><td>4</td><td>6</td><td>8</td><td>10</td></tr><tr><td>Sapphire</td><td>+Combat VIT</td><td>1</td><td>2</td><td>4</td><td>6</td><td>8</td><td>10</td></tr><tr><td>Emerald</td><td>+Combat END</td><td>1</td><td>2</td><td>4</td><td>6</td><td>8</td><td>10</td></tr><tr><td>Topaz</td><td>+Combat INT</td><td>1</td><td>2</td><td>4</td><td>6</td><td>8</td><td>10</td></tr><tr><td>Amethyst</td><td>+Combat WIL</td><td>1</td><td>2</td><td>4</td><td>6</td><td>8</td><td>10</td></tr><tr><td>Diamond</td><td>+Crit Rate</td><td>1%</td><td>2%</td><td>3%</td><td>5%</td><td>7%</td><td>10%</td></tr><tr><td>Bronze</td><td>+Equipment HP</td><td>10%</td><td>20%</td><td>35%</td><td>70%</td><td>100%</td><td>-</td></tr><tr><td>Mithril</td><td>+Equipment DEF</td><td>10%</td><td>20%</td><td>35%</td><td>70%</td><td>100%</td><td>-</td></tr><tr><td>Gold</td><td>+Equipment RES</td><td>10%</td><td>20%</td><td>35%</td><td>70%</td><td>100%</td><td>-</td></tr><tr><td>Lavenderite</td><td>+Equipment ATK</td><td>10%</td><td>20%</td><td>35%</td><td>70%</td><td>100%</td><td>-</td></tr><tr><td>Obsidian</td><td>+Equipment Skill DMG</td><td>10%</td><td>20%</td><td>35%</td><td>70%</td><td>100%</td><td>-</td></tr></tbody></table>

{% hint style="info" %}
\+Equipment X Bonuses will multiply stats from ALL equipment pieces only (doesn't factor your hero's stats)\
\
etc. Equipment for your hero with 100 HP in total gives: \
\+10 HP,  +10% HP,  +100% Equipment HP Bonus\


Bonus from equipment (+ 10 HP + 10% Hero HP) = 10 + 10 = 20. Bonus from Equipment HP Bonus = ^ x 100% = 20. Your hero's HP should be 100 + 20 + 20 = 140.
{% endhint %}

### Armor (Circlets, Armguards)

<table><thead><tr><th width="139">Relic Type</th><th width="162">Infusion Effect</th><th width="74">T1</th><th width="66">T2</th><th width="79">T3</th><th width="71">T4</th><th width="73">T5</th><th>T6</th></tr></thead><tbody><tr><td>Ruby</td><td>+DoT Resistance</td><td>1%</td><td>2%</td><td>5%</td><td>8%</td><td>10%</td><td>15%</td></tr><tr><td>Sapphire</td><td>+Armor (reduce flat damage)</td><td>1</td><td>2</td><td>4</td><td>7</td><td>11</td><td>15</td></tr><tr><td>Emerald</td><td>+Flat HP Regeneration</td><td>1</td><td>2</td><td>4</td><td>6</td><td>8</td><td>10</td></tr><tr><td>Topaz</td><td>+Life Drain</td><td>0.2%</td><td>0.5%</td><td>1%</td><td>1.5%</td><td>2%</td><td>2.5%</td></tr><tr><td>Amethyst</td><td>+Dodge Rate</td><td>0.5%</td><td>1%</td><td>1.5%</td><td>2.5%</td><td>3.5%</td><td>5%</td></tr><tr><td>Diamond</td><td>+Crit Resistance</td><td>5%</td><td>10%</td><td>15%</td><td>25%</td><td>35%</td><td>50%</td></tr><tr><td>Bronze</td><td>+Max HP</td><td>1%</td><td>2%</td><td>4%</td><td>7%</td><td>12%</td><td>-</td></tr><tr><td>Mithril</td><td>+Skill Defense</td><td>1%</td><td>2%</td><td>3.5%</td><td>7%</td><td>10%</td><td>-</td></tr><tr><td>Gold</td><td>+Damage Reflect</td><td>1%</td><td>2%</td><td>4%</td><td>7.5%</td><td>12%</td><td>-</td></tr><tr><td>Lavenderite</td><td>+Final Defense</td><td>1%</td><td>2%</td><td>3.5%</td><td>7%</td><td>10%</td><td>-</td></tr><tr><td>Obsidian</td><td>+Avoid Death<br>(survive fatal hit)</td><td>1%</td><td>2%</td><td>3%</td><td>5%</td><td>8%</td><td>-</td></tr></tbody></table>

### Weapons

To be revealed!
