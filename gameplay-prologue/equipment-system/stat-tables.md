# Stat Tables

## Main Stat

Each piece of equipment rolls one main stat when crafted, determined by its type and category. The possible range of stat is determined by the equipment's tier.

These stats can be [upgraded](./#upgrade) for greater effect using $MEDALS + $ELM up to a star level of +15.\
The number in brackets shows how much the stat can be improved when fully upgraded.

### **Charms**

<table><thead><tr><th width="196">Stat Roll</th><th width="165">T1</th><th width="150">T2</th><th>T3</th></tr></thead><tbody><tr><td>P. ATK<br>M. ATK</td><td>1 - 5 (+15)</td><td>6 - 12 (+40)</td><td>13 - 18 (+70)</td></tr></tbody></table>

### **Armguards**

<table><thead><tr><th width="200">Stat Roll</th><th width="159">T1</th><th width="150">T2</th><th>T3</th></tr></thead><tbody><tr><td><strong>HP</strong></td><td>10 - 20 (+300)</td><td>21 - 40 (+600)</td><td>41 - 60 (+1500)</td></tr></tbody></table>

### **Circlets**

<table><thead><tr><th width="202">Stat Roll</th><th width="159">T1</th><th width="152">T2</th><th>T3</th></tr></thead><tbody><tr><td>HP</td><td>10 - 20 (+300)</td><td>21 - 40 (+600)</td><td>41 - 60 (+1500)</td></tr><tr><td>HP %<br>DEF %<br>RES %<br>FINAL DMG %<br>SKILL DMG %</td><td>0.5 - 1% (+15%)</td><td>1 - 1.5% (+30%)</td><td>1.5 - 2% (+80%)</td></tr><tr><td>P.ATK<br>M.ATK</td><td>1 - 5 (+15)</td><td>6 - 12 (+40)</td><td>13 - 18 (+70)</td></tr></tbody></table>

## Sub Stats

Each piece of equipment (except skins) comes with 2 rolls of sub-stats, determined by its type and category. \
\
Players can [re-roll](./#reroll) these sub-stats using $ELM. The possible range of stat is determined by the equipment's tier. Pieces can roll the same stat lines for both rolls.

### **Charms**

<table><thead><tr><th width="194">Stat Roll</th><th width="167">T1</th><th width="145">T2</th><th>T3</th></tr></thead><tbody><tr><td>HP</td><td>10 - 20</td><td>21 - 40</td><td>41 - 60</td></tr><tr><td>HP %<br>DEF %<br>RES %</td><td>0.5 - 1.5%</td><td>1.5 - 2.5%</td><td>2.5 - 4%</td></tr><tr><td>DEF<br>RES<br>P. ATK<br>M.ATK</td><td>1-5</td><td>6-12</td><td>13-18</td></tr></tbody></table>

### **Armguards**

<table><thead><tr><th width="194">Stat Roll</th><th width="167">T1</th><th width="145">T2</th><th>T3</th></tr></thead><tbody><tr><td>HP</td><td>10 - 20</td><td>21 - 40</td><td>41 - 60</td></tr><tr><td>HP %<br>DEF %<br>RES %<br>SKILL DMG %</td><td>0.5 - 1.5%</td><td>1.5 - 2.5%</td><td>2.5 - 4%</td></tr><tr><td>DEF<br>RES</td><td>1-5</td><td>6-12</td><td>13-18</td></tr><tr><td>AGI</td><td>1-3</td><td>4-6</td><td>7-10</td></tr></tbody></table>

### Circlets

<table><thead><tr><th width="194">Stat Roll</th><th width="167">T1</th><th width="145">T2</th><th>T3</th></tr></thead><tbody><tr><td>HP</td><td>10 - 20</td><td>21 - 40</td><td>41 - 60</td></tr><tr><td>HP %<br>DEF %<br>RES %<br>FINAL DMG %<br>SKILL DMG %</td><td>0.5 - 1.5%</td><td>1.5 - 2.5%</td><td>2.5 - 4%</td></tr><tr><td>P. ATK<br>M.ATK</td><td>1-5</td><td>6-12</td><td>13-18</td></tr><tr><td>AGI</td><td>1-3</td><td>4-6</td><td>7-10</td></tr></tbody></table>
