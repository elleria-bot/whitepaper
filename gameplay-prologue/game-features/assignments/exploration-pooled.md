# Exploration (Pooled)

> _With our summoned heroes, Elleria finally has the manpower to send out scouting teams! The Apostle has scryed and revealed 4 locations containing clues necessary to find Ignacio. Stronger heroes are given the freedom to roam about the lands and explore southwards of Elleria, discovering the unknown._
>
> _Wonder what adventure lies ahead..._

Explorations emit a fixed amount of rewards shared equally by all heroes within the pool. \
Heroes sent on explorations will become busy and be unable to use other game features.\
\
Jobs have different requirements as listed below:

<table><thead><tr><th width="254">Job Title</th><th width="193">Requirements</th><th>Rewards per Day</th></tr></thead><tbody><tr><td>Necropolis Exploration</td><td>All classes<br>Level Required: 20</td><td>200 Vial of Mysterious Blood</td></tr><tr><td>Overworld Exploration</td><td>All classes<br>Level Required: 40</td><td>300 ELM Shards<br>50 $ELM</td></tr><tr><td>Ruins Exploration</td><td>All classes<br>Level Required: 50</td><td>1 Exalted Scroll<br>50 $ELM</td></tr><tr><td>Lair Exploration</td><td>All classes<br>Level Required: 60</td><td><p>1 Miracle Scroll</p><p>50 $ELM</p></td></tr></tbody></table>

Rewards emitted in each area of exploration is shared by all heroes staked in that pool! If you are the only hero staked in that pool, you will get **100%** of the rewards!

{% hint style="info" %}
Please note that emissions multiplier from hero level DOES NOT affect rewards in pooled assignments.
{% endhint %}

You can claim partial amounts of relics (etc. 0.5 Miracle Scroll), leave to quest, and come back later. **Balances will automatically be converted into a whole relic** when eligible upon claiming!

### Emissions Decay

Calculations are based on time passed, and rewards are distributed every 15 minutes.

Please note that emissions will not be generated for the first 30 minutes after sending a hero on an exploration/after claiming.

Should a hero be left in exploration for too long, the hero's efficiency will be negatively impacted, affecting his newly generated emissions. The hero's **newly distributed** emissions will be cut to 70% after \~7 days (604,800 seconds), to 40% after \~14 days (1,209,600 seconds), and 10% after \~21 days (1,814,400 seconds).

You can reset the decay by claiming rewards without quitting the job or incurring any gas fees.
