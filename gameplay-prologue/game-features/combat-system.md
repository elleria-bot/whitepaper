# Combat System

## Combat System

Our combat system caters for a full RPG experience, and more depth will gradually be added to it. Players will be rewarded for doing their research and strategizing the best way to fight for them.

The combat system in our Prologue manifests as a turn-based active battle system. Players will be able to choose actions for their heroes each turn. All of the player's active heroes will attack at once, followed by the enemy, followed by the player again...

Currently, the available actions are: **Battle, Flee** during the player's turn.

**Use Items**, and **Switch Heroes**, will be available in Phase 2 after our relics and crafting systems have been implemented and team quests are discovered.

Our formulas are all available on the Whitepaper, do contact us if anything is missing or seems outdated.

## Status Effect System

Status Effects can be positive/negative, applying buffs and debuffs to both the Heroes and the enemies. The current status effects are as follows:

| Category    | Effect Name        | Effects                                              |
| ----------- | ------------------ | ---------------------------------------------------- |
| Buff/Debuff | Attack Up/Down     | Increases your attack multiplicatively (stacks).     |
| Buff/Debuff | Agility Up/Down    | Increases your agility multiplicatively (stacks).    |
| Buff/Debuff | Defence Up/Down    | Increases your defence multiplicatively (stacks).    |
| Buff/Debuff | Resistance Up/Down | Increases your resistance multiplicatively (stacks). |
| Affliction  | Bleed              | Loses health every turn. (stacks).                   |

## Battle Actions

There are 3 battle actions available- 'Attack', 'Special Attack', and 'Defend'.

{% content-ref url="../heroes/class-skills-traits.md" %}
[class-skills-traits.md](../heroes/class-skills-traits.md)
{% endcontent-ref %}
