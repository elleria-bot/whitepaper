# Daily/Weekly Tasks

Tasks are meant to be cleared with a **single hero** without any extra effort, allowing **all players** to clear them as long as they play the game daily!

Tasks will automatically be completed as you perform actions in-game. However, make sure to claim before the week resets on Monday at 0400 UTC, or your unclaimed rewards will be lost!

You can view tasks and claim rewards from the 'Daily Tasks' button on the World Map:

<figure><img src="../../.gitbook/assets/image (6) (1) (1).png" alt=""><figcaption></figcaption></figure>

### **Reward for Daily Tasks:**

| Day       | Reward                        |
| --------- | ----------------------------- |
| Monday    | 1x Diluted Potion (Soulbound) |
| Tuesday   | 100 $MEDALS                   |
| Wednesday | 1x T1 Goblin Component        |
| Thursday  | 200 $MEDALS                   |
| Friday    | 1 Ellerium Shard              |
| Saturday  | 200 $MEDALS                   |
| Sunday    | 1x Random Elixir (Soulbound)  |

<figure><img src="../../.gitbook/assets/image (20).png" alt=""><figcaption></figcaption></figure>

### **Reward for Weekly Task:**

* 10x Ellerium Shards (Soulbound)
* 1x Lesser Positive Scroll (Soulbound) - Gives +1 to 1 random stat!

<figure><img src="../../.gitbook/assets/image (21).png" alt=""><figcaption></figcaption></figure>

### **FAQ:**

**1. When do tasks reset?**

Weekly tasks reset on Monday's reset. (when gold rush disappears)

**2. I forgot to claim for a previous day!!**

You can leave all your tasks and claim them at the end of the week (Sunday) before the weekly reset, tasks should automatically be tracked daily even if you do not open the task page.

**3. What does soulbound mean?**

The relic will be untradeable (cannot unbind from account). For scrolls, the stats will stay with your hero only within the account.
