---
description: We are more than flesh and bones.
---

# Equipment

![Take good care of your equipment, and they will take better care of you.](../../.gitbook/assets/warrior\_t2\_teaser.jpg)

As heroes venture out further, monsters become stronger and harder to defeat. Equipment will aid heroes in clearing quests by improving their combat capabilities through attributes and combat attributes (only for Quests and PvP).

We have introduced a framework for a full-fledged RPG equipment system which will be implemented after launch in different phases.

Every hero can become a legendary combatant with the right equipment.

### How to Obtain Equipment

Equipment can be crafted from the [blacksmith](../world-map.md#smithy) or earned through rewards from monsters and bosses during quests. They can also be rewarded from PvP.

### Equipment Utility

There are four main aspects to equipment, crafting, enhancing, re-rolling and infusing. Please refer below to find out more:

{% content-ref url="../equipment-system/" %}
[equipment-system](../equipment-system/)
{% endcontent-ref %}
