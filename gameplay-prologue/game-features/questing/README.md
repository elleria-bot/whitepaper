---
description: >-
  Send your heroes on explorations or quests to exterminate monsters and obtain
  rewards, equipments, and relics. The more quests completed in the world, the
  more contents unlocked.
---

# Questing

![](../../../.gitbook/assets/mage\_t2\_teaser.jpg)

Quests are an essential part of the game that rewards strong heroes with the rewards they deserve. Elleria initially only has one city that will be the base of all operations.

As the game matures and heroes exterminate enemies and bosses through quests, new cities will be unlocked through the land and guild system to expand the world and release new content.

### Quest Benefits

Quests are the core of the game as not only is the world's growth and mechanics centered around this, your token stream also mainly comes from quests.

Quests are also a source of experience, equipment, drops, and relics that will become increasingly important as the game expands.

### Quest Attempts

Each hero has a daily entry limit that resets at 1200 UTC +8. Please refer to the table below for the number of attempts possible.

| Hero Level     | Daily Attempts |
| -------------- | -------------- |
| Level 1 to 19  | 1 attempt      |
| Level 20 to 39 | 2 attempts     |
| Level 40 to 69 | 3 attempts     |
| Level 70 to 89 | 4 attempts     |
| Level 90+      | 5 attempts     |

### Combat Attributes

All heroes are eligible for quests. However, some will be better than others. Refer to [this page](../../heroes/attributes/) to see the formulas used to calculate your hero's combat attributes.

### Quest Types

Different quests will have different enemy encounter rates, monster types, monster strength, environments, and bosses. Different quests will also have different entry fees and will distribute different amounts of $ELM, $MEDALS, Drops, Equipment, and Relics.

Emergency quests or special quests will include exponentially greater rewards and a risk of permanent death (burning of your NFTs upon death). These quests will have unique indicators and alerts to warn you before you embark on these. **You will not be able to flee from these quests.**

Some quests will only give you rewards if all monsters are defeated and the quest is cleared. There will be an indicator to display this.

{% tabs %}
{% tab title="Monster Encounter Rates" %}
The number of encountered monsters and the probability of meeting different monsters will be shown individually for each quest in the quest selection page once enough information is gathered.

These rates will be updated if sufficient users share documented information and resources (screenshots, videos, community websites, etc.) to encourage social activity and an active community.
{% endtab %}

{% tab title="Rewards Table" %}
The rewards table for defeating different monsters will be shown individually for each quest on the quest selection page once enough information is gathered.

These rates will be updated if sufficient users share documented information and resources (screenshots, videos, community websites, etc.) to encourage social activity and an active community.
{% endtab %}
{% endtabs %}

### Quest Rewards

Monsters you defeat will give you EXP, $MEDALS, and possibly $ELLERIUM, and other relics.\
\
Each monster has a fixed reward table. When you defeat a monster in a quest, a random reward based on that table is generated and accumulated in your quest rewards.

When you finish the quest, all quest rewards will be added into your in-game balance if you are victorious.\
\
In some multi-encounter quests, you can keep your rewards from previous encounters even upon defeat. There will be an indicator in the quests UI to determine if your rewards will be kept.

{% hint style="info" %}
You'll be able to use 1 consumable per turn per hero within quests. Consumables will be burnt upon usage!
{% endhint %}

### Quest Difficulty

Most quests in Tales of Elleria have a range of difficulty. A higher-difficulty quest represents more rewards upon completion. Please take note of the recommended level and potential monsters that you might encounter while selecting which quest to challenge.

Difficulty can be selected by clicking on the skulls icon before embarking on quests, where it usually ranges from D1 - D3/D4.

## Region Keys

Higher quest difficulties are locked behind Region Keys. These keys are soulbound and must be crafted from the same account using [these ](../../crafting-system/crafting.md#region-key-recipes)recipes. These keys will not be consumed and users just need to have them in your inventory when attempting the quests.

<figure><img src="../../../.gitbook/assets/image (17).png" alt=""><figcaption></figcaption></figure>

Region keys required to enter quests are as follows:

<table><thead><tr><th width="178">Quest Difficulty</th><th width="146">Keys Required</th></tr></thead><tbody><tr><td><a href="https://emojipedia.org/skull">💀</a></td><td>0</td></tr><tr><td><a href="https://emojipedia.org/skull">💀</a><a href="https://emojipedia.org/skull">💀</a></td><td>1</td></tr><tr><td><a href="https://emojipedia.org/skull">💀</a><a href="https://emojipedia.org/skull">💀</a><a href="https://emojipedia.org/skull">💀</a></td><td>2</td></tr><tr><td><a href="https://emojipedia.org/skull">💀</a><a href="https://emojipedia.org/skull">💀</a><a href="https://emojipedia.org/skull">💀</a><a href="https://emojipedia.org/skull">💀</a></td><td>3</td></tr></tbody></table>

## Solo Quests

Solo Quests are generally lower-risk and lower-reward, providing a relatively stable stream of rewards for the user. These quests will only require one hero and consist of 1v1 fights against monsters until your hero runs out of HP or all monsters have been defeated.

{% tabs %}
{% tab title="Turn Order" %}
The faster skill will move first. If skill speed is tied, the one with higher agility will go first.
{% endtab %}

{% tab title="Experience & Rewards" %}
Experience and other rewards will be based on the number and types of encountered enemies.
{% endtab %}
{% endtabs %}

## Team Quests

Team Quests are generally higher-risk, higher-reward, that rewards stronger teams with higher returns. These quests require multiple heroes and consist of grouped fights against an individual monster or waves of monsters.

{% tabs %}
{% tab title="Turn Order" %}
All heroes and monsters will select a move first, and the moves will be used based on skill speed, then agility.
{% endtab %}

{% tab title="Character Line-up" %}
When sending your characters on a quest, you will be prompted to arrange their formation and line-up. Team quests can accept up to 5 characters (active + reserves) depending on the quest.

All active characters will attack at once and can be switched to a reserved character.

When a character loses all his health, the reserve next in line (if any) will replace the fallen character in his slot.
{% endtab %}

{% tab title="Experience & Rewards" %}
Experience and other rewards will be based on the number and types of encountered enemies.

All heroes will receive the full experience at the end of the fight (not split amongst participants).
{% endtab %}
{% endtabs %}

## Emergency Quests

Emergency Quests are time-limited quests that may be solo/team-based and have special requirements and special rewards. A framework has been set up to implement these special quests.

Some quests in a dangerous environment might require you to have protective charms. I wonder how we can craft those...
