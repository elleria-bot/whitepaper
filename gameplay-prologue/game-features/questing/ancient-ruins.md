---
description: >-
  Golems have been sighted in the depths of the Ancient Ruins. Hidden mysteries
  lie dormant among these ruins. Will you unravel the secrets beneath?
---

# Ancient Ruins



<figure><img src="../../../.gitbook/assets/image (52).png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
The Ancient Ruins awards $MEDALS, $ELM, Relics, ELM Shards, and EXP, allowing you to accumulate resources for Equipment enhancing. This region awards $ELM!
{% endhint %}

### Possible Encounters:

{% tabs %}
{% tab title="Peblet" %}
A tiny, rugged stone creature that roams the Ancient Ruins. The smaller ones scuttle harmlessly among the ruins, but beware of their larger kin, whose solid forms can deliver a surprisingly powerful blow!

These pebble monsters deal physical damage and are a common sight amidst the ruins. The treasures they yield vary, echoing the diverse minerals and elements they've absorbed from their stony domain.
{% endtab %}

{% tab title="Coalheart" %}
Coalheart, a towering golem shaped from ancient rock, stands guard in the deeper chambers of the Ancient Ruins. With a silhouette resembling a human form, Coalheart wields mystical forces, dealing formidable magic damage to any intruder daring enough to challenge its might.

As you navigate the perilous depths, prepare to face Coalheart – a true test of your strategic prowess and magical resilience.
{% endtab %}

{% tab title="Boulden" %}
Resembling a boulder come to life, Boulden is as durable as the ancient stones it's composed of, making it a formidable opponent in battle. \
\
Its hefty, stone-bound form dominates the landscape, a testament to the age-old magic that binds it. Boulden's sheer size makes it a slow mover, but its powerful, ground-shaking strikes can catch even the swiftest adventurers off guard with massive physical damage.
{% endtab %}

{% tab title="Gollux" %}
Gollux, the colossal guardian of the Ancient Ruins, stands as a monolithic testament to the lost era's might and mystique. This massive golem, crafted from the very bedrock of the ruins, is the ultimate physical boss monster, embodying the raw power and enduring strength of the ages.

With its towering presence and formidable power, Gollux challenges even the bravest of adventurers. Its attacks are not just physically overwhelming but are strategically executed, demanding both skill and cunning from those who dare to confront it.&#x20;
{% endtab %}

{% tab title="???" %}
???
{% endtab %}
{% endtabs %}

### Uncovered Areas of Interest:

**Ancient Ruins - Peblet's Path**

In the Ancient Ruins, Peblet's Path unfolds, a trail lined with small stone creatures known as Pebblets. These mini golems, born from the ruins, possess a curious resilience. Navigating this path offers an initial taste of the deeper mysteries and ancient powers that the Ruins conceal.

Preparation Fees: 500 $MEDALS\
Heroes Required: 1\
Difficulty levels: 3

**Ancient Ruins - Boulden's Border**

At Boulden's Border in the Ancient Ruins, golems have gathered, forming unexpected barriers. Energized by the ruins, they challenge anyone trying to delve deeper. Your task is to navigate these protective golems and secure safe passage into the heart of the ruins. Proceed with caution!

Preparation Fees: 500 $MEDALS\
Heroes Required: 1\
Difficulty levels: 4

#### Ancient Ruins - Golem's Grudge

In the deepest parts of the Ancient Ruins, some golems have absorbed immense magical energies, evolving into formidable beings. These enhanced golems, fueled by ancient grudges, are now menacingly patrolling the ruins, posing a significant threat to all who venture too close. It's crucial to confront and pacify these guardians before their power grows uncontrollable.

Preparation Fees: 1000 $MEDALS\
Heroes Required: 1\
Difficulty levels: 4
