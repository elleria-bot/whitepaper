---
description: >-
  Goblins have been spotted on the plains just outside town, chasing away
  animals and causing unease. Show them who is boss!
---

# Goblin Plains

![](../../../.gitbook/assets/gcamp\_all\_long.png)

{% hint style="info" %}
The Goblin Plains award $MEDALS, Relics, ELM Shards, and EXP, allowing you to accumulate resources to upgrade your heroes and craft consumables.
{% endhint %}

### Possible Encounters:

{% tabs %}
{% tab title="Goblin Warrior" %}
A little green creature wielding a rudimentary club. Some of the townspeople even find them cute and harmless, but you very well know their dangers.\
\
Deals physical damage and can be found scouting around the plains.
{% endtab %}

{% tab title="Hopgoblin" %}
A green creature covered in armour, wielding a dangerous-looking weapon. Does the presence of armour mean these creatures have society and intelligence? And where did they come from, how did they train, and why are they attacking the citizens?\
\
It deals physical damage and can mostly be found within encampments. They're rarely sighted on the outskirts, most likely only when they're checking in on the scouts.
{% endtab %}

{% tab title="Goblin Mage" %}
Also a green creature, but covered in bone armour. Something ephemeral enhances it, giving it the capability to conjure and manipulate bones around him.

It deals magical damage and can be found throughout the plains, acting as an advisor for different camps. Strong but fragile, beware of his magical attacks, especially in group fights.
{% endtab %}

{% tab title="Goblin Leader" %}
D kxjh juhhq fuhdwxuh zlhoglqj d jldqw dah. EHZDUH, pdqb dgyhqwxuhuv kdyh ehhq lqmxuhg vlpsob dwwhpswlqj wr revhuyh lw. Ghvslwh lwv vlch, lw erdvwv vshhg dqg dqg kdv txlfn uhiohahv.

Ghdov kljk dprxqwv ri skbvlfdo gdpdjh, dqg krshixoob wkhb uhpdlq zlwklq wkhlu hqfdpsphqwv.
{% endtab %}

{% tab title="Goblin Ogre" %}
D pxwdwhg ohdghu wkdw kdv zrnh xs dqg fkrvh ylrohqfh. Wkhvh rjuhv ryhuzkhop rwkhuv zlwk euxwh irufh, bhw riihu olwwoh wr qrwklqj lq uhwxuq.

Erwk vwurqj dqg gxudeoh, wkhvh irhv duh wrxjk wr ehdw dqg duh d elj phqdqfh wr Hoohuld. Zlsh wkhp rxw dw doo frvwv.
{% endtab %}
{% endtabs %}

### Uncovered Areas of Interest:

#### Goblin Plains - Adventure Begins!

Our scouts have managed to capture some of the weaker goblins for research and training. Face off against one of these to get a feel for battle and to boost your confidence and spirit for adventuring!\
\
Preparation Fees: None!\
Heroes Required: 1\
Difficulty levels: 3

#### Goblin Plains - Outskirts

The further you wander, the more goblins start to appear. Be careful not to get overwhelmed. You might still be on the outskirts, but should their scouting paths overlap...

Preparation Fees: 500 $MEDALS\
Heroes Required: 1\
Difficulty Levels: 4

#### Goblin Plains - Outpost

Stealthily tracking the scouts, you've chanced upon one of their outposts. There seems to be a commander giving orders to the scouting teams. Take him out and the other goblins might scatter! Beware: You'd have to drop all your loot to retreat out of there. It's dangerous, make sure you're prepared!

Preparation Fees: 1000 $MEDALS\
Heroes Required: 1\
Difficulty Levels: 4

#### Special - Gold Rush (Limited Quest!)

Goblins love to hoard and collect items from all around, it's no wonder their camps are such a mess. What happens if a goblin stays exposed to medals and ellerium for a prolonged period of time...?

Preparation Fees: 500 $MEDALS\
Heroes Required: 1

{% hint style="info" %}
This quest is only available on weekends! You can find it under the 'Special' Region.
{% endhint %}
