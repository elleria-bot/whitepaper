# Soul-Bound Attributes

## Soul-bound Mechanism

Heroes can obtain soul-bound stats through the usage of soul-bound scrolls.

Soul-bound Stats count towards both assignment requirements and combat stats.

These stats are tied to your wallet and hero and are shown in brackets. If you transfer your hero to another wallet, the soul-bound stats will be lost TEMPORARILY. However...

If the hero was [REBIRTHED](../../game-features/hero-rebirth.md), all soul-bound stats for all wallets will be lost.\
\
_For example:_\
_- 0xA has +1 STR on Hero #1._\
_- Hero #1 is transferred to 0xB._\
_- 0xB uses a soul-bound scroll and gets -2 STR on Hero #1._\
_- 0xB rebirths Hero #1._\
_- Both 0xA and 0xB's soul-bound stats will be reset._

_---_

If the hero wasn't REBIRTHED, your soul-bound stats will re-appear once the hero is transferred back to you.\
\
_For example:_\
\- 0xA has +1 STR on Hero #1.\
\- Hero #1 is transferred to 0xBBB.\
\- 0xB uses a soul-bound scroll and gets -2 STR on Hero #1.\
\- 0xB transfers Hero #1 back to 0xA.\
\- 0xA will see his original +1 STR.\
\- If transferred back to 0xB, 0xB will see his soul-bound -2 STR as well.

### Visualizing Soul-bound Stats

You can see soul-bound stats from your hero's card in the hero inventory.

{% hint style="info" %}
90 (+1) means that you have 89 permanent stats and 1 soul-bound stat.
{% endhint %}

![](<../../../.gitbook/assets/image (1) (3).png>)

### Sources

Soul-bound stats can be obtained by using soul-bound scrolls on heroes! You can acquire these scrolls from crafting, or from in-game content such as Weekly Tasks and Tower of Babel.
