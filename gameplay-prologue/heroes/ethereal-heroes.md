# Ethereal Heroes

Welcome to a revolutionary turning point in the magical realm of Elleria! This game-changing addition aims to redefine how Free-to-Play (F2P) players engage with our universe and provide an avenue for player progression.

Unlike any hero known before, the Ethereal Heroes are uniquely soul-bound (untradable). Linked to the ethers of their summoner’s wallet, they symbolize a profound connection, an inseparable bond echoing the unbreakable nature of their untradeable existence.

<figure><img src="../../.gitbook/assets/image (27).png" alt="" width="375"><figcaption></figcaption></figure>

## What are Ethereal Heroes?

Ethereal Heroes are mystical entities designed to help onboard new players into the Tales of Elleria ecosystem. They act as a gateway for those wanting to “try” the game before fully immersing themselves in the world of Elleria. With Ethereal Heroes, players can experience the game wholly and progress their team using the Ethereal Hero Shop, effectively bridging the gap between newcomers and seasoned players.

## Features&#x20;

Ethereal Heroes come with a unique set of features that separate them from the standard heroes in Tales of Elleria.

Ethereal Heroes **CAN**:

* Earn $MEDALS from assignments.
* Earn ELM Shards from quests.
* Quest all quests normally, except they cannot earn $ELM and Relics.
* Be levelled, upgraded using scrolls, and renamed.
* Embark on one quest a day regardless of level.
* Enter the Tower of Babel with two free attempts a cycle.

However, Ethereal Heroes **CANNOT**:

* Be sold.
* Earn $ELM and Tradable Relics.
* Be rebirthed or used to rebirth other heroes.
* Appear in the Marketplace.
* Earn scores on the Tower of Babel Leaderboard.

## Summoning Ethereal Heroes

The first Ethereal Hero can be summoned for free, opening up the world of Elleria for all players.&#x20;

![](<../../.gitbook/assets/image (30).png>)

<figure><img src="../../.gitbook/assets/image (29).png" alt=""><figcaption></figcaption></figure>

Players who would like more Ethereal Heroes can summon them as follows:



<table><thead><tr><th width="194.33333333333331">Hero</th><th width="161">Cost</th><th>Summoning Bonus</th></tr></thead><tbody><tr><td>1st Ethereal Hero</td><td>FREE</td><td>-</td></tr><tr><td>2nd Ethereal Hero</td><td>50 $MAGIC</td><td>20,000 $MEDALS</td></tr><tr><td>3rd Ethereal Hero</td><td>100 $MAGIC</td><td>50,000 $MEDALS + 5x Fading Memories</td></tr><tr><td>4th Ethereal Hero</td><td>250 $MAGIC</td><td>100,000 $MEDALS + 2x Vivid Memories</td></tr><tr><td>5th Ethereal Hero</td><td>400 $MAGIC</td><td>Hero Summoned is a Class-Specific Hero<br>(86-90 main stat)</td></tr></tbody></table>



Ethereal heroes summoned are randomly selected from the genesis classes (Warrior, Ranger, Assassin, Mage). They also have a maximum summonable primary stat of 80 and maximum summonable total attributes of 376. If you wish to enhance your Ethereal Hero, scrolls can be used for improvement to reach a maximum attribute of 445.

A maximum of 5 Ethereal Heroes can be summoned. Beginning with the second Ethereal Hero, a bonus reward will be granted for each additional summoning. The fifth hero summoned will automatically be a Class-Specific Hero, boasting a main between stat of 86–90, a minimum sub stat of 61, and a maximum total of 376 stats.

{% hint style="info" %}
Class-Specific Heroes are Heroes that qualifies for Class-Specific assignments, and can earn $MEDALS at a **HIGHER RATE** than regular heroes! Click [here ](../game-features/assignments/assignments-fixed.md)to find out more.
{% endhint %}

### Ethereal Hero Quests

<figure><img src="../../.gitbook/assets/image (31).png" alt=""><figcaption></figcaption></figure>

Ethereal Heroes can undertake a unique quest explicitly designed for them once a day. This free-entry quest allows heroes to fight monsters that drop a special currency called Soul Dust. Each monster defeated will drop 1–10 Soul Dust and one Wooden Lootbox.

### Wooden Lootbox

<figure><img src="../../.gitbook/assets/image (32).png" alt=""><figcaption></figcaption></figure>

Each successful completion of the Ethereal Quest grants you one Wooden Lootbox. These loot boxes can be opened for a variety of rewards ranging from $MEDALS, Shards, Soul Dust, Potions and Scrolls depending on chance.&#x20;

<figure><img src="../../.gitbook/assets/ezgif.com-video-to-gif.gif" alt=""><figcaption></figcaption></figure>

Rates for the Wooden Box are disclosed as follows:\


<table data-full-width="false"><thead><tr><th width="267.33333333333337">Item (Soulbound)</th><th width="169">Probability</th></tr></thead><tbody><tr><td>Miracle Scroll</td><td>0.001%</td></tr><tr><td>Blessing Scroll</td><td>0.002%</td></tr><tr><td>Lesser Positive Scroll</td><td>0.02%</td></tr><tr><td>100 $MEDALS</td><td>20.00%</td></tr><tr><td>250 $MEDALS</td><td>8.00%</td></tr><tr><td>500 $MEDALS</td><td>3.00%</td></tr><tr><td>1000 $MEDALS</td><td>0.50%</td></tr><tr><td>2500 $MEDALS</td><td>0.15%</td></tr><tr><td>5000 $MEDALS</td><td>0.10%</td></tr><tr><td>10000 $MEDALS</td><td>0.05%</td></tr><tr><td>2x Soul Dust</td><td>20.00%</td></tr><tr><td>5x Soul Dust</td><td>8.00%</td></tr><tr><td>10x Soul Dust</td><td>3.00%</td></tr><tr><td>20x Soul Dust</td><td>0.25%</td></tr><tr><td>40x Soul Dust</td><td>0.05%</td></tr><tr><td>Diluted Recovery Vial</td><td>2.08%</td></tr><tr><td>Diluted Recovery Potion</td><td>1.00%</td></tr><tr><td>Warrior Elixir</td><td>0.50%</td></tr><tr><td>Assassin Elixir</td><td>0.50%</td></tr><tr><td>Mage Elixir</td><td>0.50%</td></tr><tr><td>Ranger Elixir</td><td>0.50%</td></tr><tr><td>Luck Elixir</td><td>0.50%</td></tr><tr><td>1x Ellerium Shard</td><td>20.00%</td></tr><tr><td>2x Ellerium Shard</td><td>8.00%</td></tr><tr><td>4x Ellerium Shard</td><td>3.00%</td></tr><tr><td>8x Ellerium Shard</td><td>0.25%</td></tr><tr><td>16x Ellerium Shard</td><td>0.05%</td></tr></tbody></table>

### The Soul Dust Shop

<figure><img src="../../.gitbook/assets/image (33).png" alt="" width="375"><figcaption></figcaption></figure>

Soul Dust, earned from the Ethereal Hero quests, can be used in the Ethereal Emporium (Soul Dust Shop) to purchase soul-bound items. This shop is located within the Adventurer’s Guild.





## Conclusion <a href="#1e9f" id="1e9f"></a>

The introduction of Ethereal Heroes marks an exciting phase in Tales of Elleria. We’re eager to see how these heroes will help onboard new players and add depth to our game. As a player, you can look forward to exploring the realm of Elleria through a different perspective, encountering quests, collecting relics, and experiencing the thrill of the game.

Remember, the journey in Elleria doesn’t end; it evolves. We’re excited to bring more features and opportunities to you. With the Ethereal Heroes as your gateway, may you find grand adventures, valuable allies, and timeless stories.

