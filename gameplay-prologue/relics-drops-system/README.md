# Relics/Drops System

![](<../../.gitbook/assets/image (12) (1) (1) (1).png>)

_From Goblin's Foot to Shiny Amethyst and Blessed Scrolls._

Relics can be obtained through various sources in the game. Through merchants, quests, and monsters, you'll be able to obtain monster drops, crafting components, and even artifacts.

You can dismantle/refine/upgrade items through the crafting system and transmute them into powerful scrolls, consumables, equipment, accessories, and more!

## Crafting Gameplay Loop

![](<../../.gitbook/assets/image (1) (3) (1).png>)

In Tales of Elleria, we want to add as much depth as we can to the crafting system yet, at the same time, make it easy for new players to learn. We have come up with a crafting gameplay loop to simplify the entire relics system.

1. Go on quests to obtain monster drops
2. Dismantle monster drops into basic components (Upgrade basic components if needed)
3. Craft consumables, equipment or artifacts.
4. Wear equipment, or use your consumables and artifacts wherever needed.

## Types of Items

Items can be classified into 5 different categories and 6 different tiers of rarities according to their border color. Each type of item has its unique purpose within Elleria. They are:

* Crafting Components
* Artifacts
* Consumables
* Upgrade Materials
* Misc. Items

Please check the subpages for more information.

{% content-ref url="crafting-components.md" %}
[crafting-components.md](crafting-components.md)
{% endcontent-ref %}

{% content-ref url="artifacts-consumables.md" %}
[artifacts-consumables.md](artifacts-consumables.md)
{% endcontent-ref %}

{% content-ref url="upgrade-materials.md" %}
[upgrade-materials.md](upgrade-materials.md)
{% endcontent-ref %}

{% content-ref url="miscellaneous-items.md" %}
[miscellaneous-items.md](miscellaneous-items.md)
{% endcontent-ref %}

## **Soulbound Relics**

**To hasten players' progression and offer rewards without allowing the economy to be exploited by bots, some relics will be soulbound, indicated by a 'lock' icon on the top right. This means that the relic can only be used in-game, and cannot be bridged out/in.**&#x20;

**The balance below indicates \<TOTAL OWNED (Soulbound quantity owned)>.**\
**In the image below, the user owns 50 Soulbound Magic Shards.**

<figure><img src="../../.gitbook/assets/image (50).png" alt=""><figcaption></figcaption></figure>



{% hint style="info" %}
Soulbound scrolls will add soulbound stats to heroes. These stats are indicated in brackets () and will be lost when the hero is transferred to another wallet, and returned when the hero is transferred back.
{% endhint %}
