# Crafting Components

With the appearance of monsters also comes a huge variety of new resources and materials never seen before. With the apostle's help, Elleria's craftsmen have managed to derive uses for monster drops and parts.

While some items might look unappealing, useless, or gross; Behold, a skilled craftsman might be able to process and create something invaluable out of them.

The craftsmen are continually trying to find different ways to process drops and make crafting more efficient. Perhaps in the future other methods might be discovered.

## Monster Drops

![Goblin Drops](<../../.gitbook/assets/image (16) (1) (1).png>)

These parts can be salvaged from monsters after defeating them. While these are most likely unusable directly, they can be processed by a craftsman and dismantled into usable components.

Monster Drops > Dismantle into Base Components > Use for Crafting.

You can obtain different types of monster drops, each of which will be dismantled into various usable components:

* Armour
* Artisanal (monster parts)
* Weapons
* Dolls

## Base Components

![](<../../.gitbook/assets/image (7) (1) (1) (1).png>)

Base components are your staple building blocks within the crafting system. Players who want to focus on crafting should have plenty of these in stock.

* Some base components can be combined with other components to refine them.
* Some base components can be combined according to a recipe to create an item.

<table><thead><tr><th width="176.81785356551143">Base Component</th><th>Description</th></tr></thead><tbody><tr><td>Liquid Vials </td><td>Vials containing different mysterious liquids. Handle with care!</td></tr><tr><td>Parchments</td><td>Thick paper infused with the properties of the materials used to create them.</td></tr><tr><td>Bone Powder</td><td>Purified powder made from processing monster bones. They seem to have a faint aura..</td></tr><tr><td>Cloth Strips</td><td>Durable cloth obtained from monsters. It's unknown what was used to create these.</td></tr><tr><td>Wood Chips</td><td>Chips of wood infused with the monster's magical properties. Do not ignite!</td></tr><tr><td>Leather</td><td>Leather processed from monster parts. Eerie but effective..</td></tr><tr><td>Magic Shards</td><td>Crystallized form of Magic that seeped in from an alternate universe. Seems to contain legendary power.</td></tr></tbody></table>
