# Miscellaneous Items

![](<../../.gitbook/assets/image (15) (1).png>)

Throughout your adventure in Elleria, you might pick up or find mysterious items whose purposes vary...

Hold on tight to them, for someday, you might discover its use.

These items include:

* Tickets which can be exchanged/used in various ways
* Vanity & Collector items
* ??????
* Broken Components
* Item Pieces
* Regional Keys
* ... and more
