---
description: ⚠️ Breaking News from Elleria's Depths! ⚠️
---

# World Boss: Defi Kingdom Bloater (Season 10)

_The legendary blobfish, Bloater from DeFi Kingdoms has made a splash in Elleria’s serene waters, causing chaos among our fishermen! Sharpen your swords, gather your allies, and prepare to dive into an unforgettable clash with this colossal, enigmatic creature!_

<figure><img src="../.gitbook/assets/image (6).png" alt=""><figcaption><p>*dramatic sound effect*</p></figcaption></figure>

{% hint style="success" %}
## Up to 35,000 USD in prizes are up for grabs! Read to find out how you can participate.
{% endhint %}

{% hint style="success" %}
**New to Tales of Elleria?** Fret not, there are generous rewards you can obtain to catch up on the leaderboard by holding **Defi Kingdom Heroes and Participating Treasure Ecosystem NFTs.** Check out these [Exclusive Perks](world-boss-defi-kingdom-bloater-season-10.md#exclusive-partnership-perks) here.
{% endhint %}

## Cross-Ecosystem Collaboration

Elleria has always welcomed collaborations that not only amplify the gaming experience but also knit virtual worlds closer. First Pixelmon, now Defi Kingdoms.

The introduction of _Bloater_, a lovable character from Defi Kingdoms, into Tales of Elleria, accompanies our narrative of cross-IP development to expose players to different ecosystems. Our teams have spent months behind the scenes to make this partnership happen, resulting in the birth of a Bloater in Elleria's style, complete with unique animations and effects. Blub blub!

<figure><img src="../.gitbook/assets/ezgif.com-video-to-gif.gif (1).gif" alt=""><figcaption></figcaption></figure>

### Event Info

<figure><img src="../.gitbook/assets/image (8).png" alt=""><figcaption></figcaption></figure>

The event will take place in Tales of Elleria, as a special World Boss you can send ToE heroes to fight against. You can access the event via [https://app.talesofelleria.com/](https://app.talesofelleria.com/) and log in with your Metamask wallet.

**Duration:** 18th December 2023 to 1st January 2024 (4AM UTC).\
**Rewards:** 25,000 $ARB and 250,000 $CRYSTAL will be given out to participants based on the boss leaderboards.\
**Restrictions:** Entries are limited by the number of quest attempts and World Boss Tickets you have. To get more entries, summon/buy more heroes and make sure you have sufficient tickets per entry.

To learn more about obtaining a hero and starting, please refer to our [quickstart guide](../welcome-to-elleria/guides/general-game-guide.md) or ask in our Discord! An overview is also available in the next section.

## Dive Deep into the World Boss Structure

### **Step 1: Obtaining a Hero**

<figure><img src="https://cdn-images-1.medium.com/max/800/1*XPM9Ivfuh-O9wvpexulvtw.png" alt=""><figcaption></figcaption></figure>

Before you can challenge the Bloater, you’ll need a hero by your side. For newcomers to Tales of Elleria, you can obtain a [free hero](https://medium.com/@talesofelleria/ethereal-heroes-a-new-era-5eef5b2b9c82) through the summoning altar, or purchase a tradable hero from the [trove marketplace](https://app.treasure.lol/collection/tales-of-elleria). Assemble your team and prepare them for the grand battle that awaits!

### **Step 2: Securing Entry Tickets**

<figure><img src="https://cdn-images-1.medium.com/max/800/0*glo9govE5sO1jvAE" alt=""><figcaption></figcaption></figure>

You will require an entry ticket for every attempt you challenge the Bloater. Hoard these tickets by crafting them using quest item drops, purchasing from [Trove Marketplace](https://app.treasure.lol/collection/tales-of-elleria-relics/20000), or procuring from the In-Game's Adventurer's Guild Shop.

<figure><img src="../.gitbook/assets/image (9).png" alt=""><figcaption></figcaption></figure>

> Hint: It is cheaper to [craft your entry tickets](https://docs.talesofelleria.com/gameplay-prologue/crafting-system/crafting#consumable-recipes) than purchase them from Trove marketplace or from the adventurer’s shop!

### **Step 3: Engage in Battle**

In this boss fight, heroes will attempt to deal as much damage as possible before running out of HP. The **total amount of damage done toward the Bloater across all heroes and attempts in a wallet** will be ranked on the leaderboard where prizes will be allocated.

A player can send an unlimited number of heroes into the world boss if he has sufficient entry tickets and quest attempts to rack up more points.

#### **Battle Mechanics**

* The battle utilizes a turn-based system, and the boss can either use a special/normal attack per turn. A special attack will hit for a minimum of 15% of the hero's HP.
* Each day, there is a fixed attack pattern for the first ten turns. Attacks after the 10th turn are randomized.
* The longer the battle, the more empowered the Bloater will be. Beyond the 10th turn, the boss will have progressively increasing stats.
* You can use one item (potions/elixir) per turn
* A quest must be completed for its damage to count. Complete all quests before the final cut-off time!
* A hero must remain in your wallet for its score to count. If the hero quests on a different account, all previous quest attempts are voided. (If the hero moves from A > B > A, all attempts from the original A before transfer are also voided)
* The Bloater does Magical damage and has fixed Base Stats throughout the event.

<figure><img src="../.gitbook/assets/ezgif.com-crop.gif" alt=""><figcaption></figcaption></figure>

## Rewards Await the Brave

In this event, there are rewards to the 1000th player and a huge reward pool of **25,000 $ARB and 250,000 $CRYSTAL**. 200 $ARB will be distributed randomly via a raffle, and the rest of these rewards will be distributed based on the boss leaderboard. Place high to receive more!

### $ARB Rewards

1st Place ► 1000 $ARB\
2nd Place ► 750 $ARB \
3rd Place ► 500 $ARB \
4–10 Place ► 300 $ARB \
11–25th Place ► 200 $ARB \
26–50th Place ► 150 $ARB \
51–100th Place ► 120 $ARB \
101–200th Place ► 25 $ARB \
201–600th Place ►10 $ARB \
601–1000th Place ► 3 $ARB

{% hint style="info" %}
You will be able to claim your $ARB leaderboard rewards shortly after the event ends from within the game.
{% endhint %}

### CRYSTAL Rewards

<figure><img src="../.gitbook/assets/image (11).png" alt=""><figcaption></figcaption></figure>

DeFi Kingdoms have also allocated **250,000 CRYSTALS** in vouchers to be won in this campaign! Here is the breakdown:\
\
1-100th Place ►1000 CRYSTAL\
101–200th Place ►600 CRYSTAL\
201–300th Place ► 300 CRYSTAL\
301–900th Place ► 100 CRYSTAL

These crystals can be used to purchase DFK NFTs on their [Crystalvale Marketplace](https://defikingdoms.com/crystalvale) on Avalanche! Try out their game to earn some highly demanded $JEWEL tokens!

### $ARB Raffle

Each attempt at the world boss rewards players with a special **Proof of Bloater Trophy**, a soulbound relic signifying their valor.

<div align="center">

<figure><img src="../.gitbook/assets/image (10).png" alt="" width="188"><figcaption></figcaption></figure>

</div>

Each trophy represents an entry into the 200 $ARB raffle, where 40 $ARB will be distributed to 5 random participants! This 40 $ARB will be distributed directly to the winners shortly after the event ends.

## Exclusive Partnership Perks

{% hint style="info" %}
All mail will be distributed around 18th December 2023, since we have to process and aggregate the various snapshots given before. Thank you for your understanding!
{% endhint %}

### **Defi Kingdom Heroes (both Serendale and Crystalvale)**

Hold a DFK Hero? A snapshot will be taken between 15th and 16th December for wallets holding at least 1 DFK Hero. These wallets will receive some GOODIES that can boost your damage in the leaderboards. Sign in with the wallets holding DFK Heroes and you will see a mail with your rewards.\
\
Players will be eligible by wallet, so each wallet will receive one set of holder rewards regardless of how many DFK Heroes they own.

<figure><img src="../.gitbook/assets/image (12).png" alt=""><figcaption></figcaption></figure>

### Ecosystem Partners

Heya Treasure Fam! Hold an NFT from one of these collections? The wallet holding the NFT will receive a World Boss Ticket that you can use for a free attempt at the World Boss! \
\
Players will be eligible by wallet, so each wallet will receive 1x World Boss Ticket regardless of how many different NFTs/Collections they own.

**Kaiju Cards:** Genesis Kaiju, Pioneer Kaiju, Adventurers\
**Mighty Bear Games**: MightyNet Genesis Pass, Big Bear Syndicate\
**Smols:**  Smol Brains, Smol Bodies, Smol Jrs, Swol Jrs\
**Bridgeworld:** Genesis Legions, Auxiliary Legions\
**Realm**: Realms, Adventurer of the Voids\
**Knights of the Ether**: Knights, Initiated Knights\
**Battlefly:** Battlefly\
**Beacon:** Pets, Founding Characters, Items\
**Bitmates:** Mates

A total of \~42k wallets are eligible! Check your wallet eligibility [here](https://docs.google.com/spreadsheets/d/1nA4vGm8\_54i8UWD\_X7NwhBxaCKUUWq1q196TTMJTP74/edit#gid=1966396839)!

{% hint style="success" %}
ALL players can also mint 1 Free Ethereal Hero per account, meaning you will be able to participate in the World Boss Event for free to have a chance at the raffle and prizes!
{% endhint %}

## Join the Adventure

The Bloater challenge is not just a battle; it’s a test of our collective strength and unity. Prepare for a breathtaking collision of worlds, where the stakes are high, and the rewards are grand. Join us in this monumental event and etch your name in the annals of Elleria’s history.

Stay tuned for more thrilling updates and thank you for being a part of the evolving journey of Tales of Elleria.
