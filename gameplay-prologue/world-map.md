# World Map

## Summoning Altar

![](<../.gitbook/assets/summoning\_circle\_reveal (1).jpg>)

## Hero's Quarters

![](<../.gitbook/assets/HQ\_reveal\_variant2 (1).jpg>)

## Adventurer's Guild

![](<../.gitbook/assets/tavern\_reveal (1) (1).png>)

## City Gates (Quests)

![](../.gitbook/assets/gate\_reveal.png)

## Bank

![](../.gitbook/assets/bank\_reveal.png)

## Wishing Well

![](../.gitbook/assets/wishingwell\_reveal.png)

## Castle

![](../.gitbook/assets/castle\_reveal.png)

## Coliseum

![](../.gitbook/assets/coliseum\_reveal.png)
