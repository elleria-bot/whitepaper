# Collaborations

Tales of Elleria is actively looking for Games/Projects to partner up with us!&#x20;

Please head to Discord and open a ticket for collaboration opportunities!&#x20;

## Tales of Elleria x Defi Kingdoms

Cross-ecosystem World Boss event

<figure><img src="../.gitbook/assets/image (6).png" alt=""><figcaption><p>*dramatic sound effect*</p></figcaption></figure>

{% content-ref url="../gameplay-prologue/world-boss-defi-kingdom-bloater-season-10.md" %}
[world-boss-defi-kingdom-bloater-season-10.md](../gameplay-prologue/world-boss-defi-kingdom-bloater-season-10.md)
{% endcontent-ref %}

## Tales of Elleria x Pixelmon

Cross-ecosystem World Boss event

<figure><img src="../.gitbook/assets/image (14).png" alt=""><figcaption><p>Pixelmon Kevin invades Elleria! </p></figcaption></figure>



{% content-ref url="../references/archived-world-boss-mechanics/world-boss-pixelmon-ft.-kevin-season-9.md" %}
[world-boss-pixelmon-ft.-kevin-season-9.md](../references/archived-world-boss-mechanics/world-boss-pixelmon-ft.-kevin-season-9.md)
{% endcontent-ref %}
