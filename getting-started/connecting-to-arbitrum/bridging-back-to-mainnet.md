# Bridging back to Mainnet

## **How do I bridge ETH back from Arbitrum to mainnet?**

There are a number of options to bridge ETH from Arbitrum to Mainnet:

* Arbitrum Bridge (Takes 7 Days): [https://bridge.arbitrum.io/](https://bridge.arbitrum.io)
* Synapse (\~30 minutes): [https://synapseprotocol.com/](https://synapseprotocol.com/)
* Hop Exchange (\~30 minutes): [https://hop.exchange/](https://hop.exchange/)
* Celer (\~30 minutes): [https://cbridge.celer.network/#/transfer](https://cbridge.celer.network/#/transfer)

{% hint style="info" %}
**Note:** These options are referenced from TreasureDAO's whitepaper. Please do your own research too before proceeding!
{% endhint %}
