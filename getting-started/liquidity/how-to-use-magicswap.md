# How to use MagicSwap

MagicSwap acts as a gateway to the cross-game ecosystem, built for and by the[ Treasure DAO](https://twitter.com/Treasure\_DAO) community. Use MagicSwap to obtain $ELM, sell $ELM, and provide liquidity.

## How to Swap $MAGIC to $ELM

1. Head to [https://magicswap.lol/?input=MAGIC\&output=ELM](https://magicswap.lol/?input=MAGIC\&output=ELM)
2. Connect to your wallet holding either $MAGIC that you would like to swap
3. Enter the amount of $MAGIC you would like to swap to

<figure><img src="../../.gitbook/assets/image (9) (1) (3).png" alt=""><figcaption></figcaption></figure>

4\. Press swap and approve your $MAGIC to swap, then sign the transaction

<img src="../../.gitbook/assets/image (24) (1).png" alt="" data-size="original">

5\. Click on confirm swap and pay gas to transact

![](<../../.gitbook/assets/image (23) (1).png>)

Congratulations! You now have $ELM.

## How to Provide Liquidity

1. Ensure that you have sufficient $ELM and $MAGIC in your wallet that you would like to pair\
   (If you do not, refer to the instructions above on how to obtain $ELM/MAGIC)
2. Head to [https://magicswap.lol/](https://magicswap.lol/) and click on "pool" at the bottom of the page
3. Enter the amount of $MAGIC/$ELM you would like to pair

<figure><img src="../../.gitbook/assets/image (22) (1).png" alt=""><figcaption><p>The pool will calculate the pair accordingly, just fill in either your desired $ELM or $MAGIC amount</p></figcaption></figure>

4\. Click on 'Add Liquidity' and execute the transaction to provide liquidity and obtain LP tokens.\
(Note: You will need to execute 2 transactions approving $MAGIC and $ELM if it is your first time adding liquidity)

5\. Congratulations! You have now provided Liquidity and own MagicSwap LP tokens

![](<../../.gitbook/assets/image (16) (1).png>)

### Read more on how you can use MagicSwap LP tokens in Elleria!

{% content-ref url="usdelm-magic-lp.md" %}
[usdelm-magic-lp.md](usdelm-magic-lp.md)
{% endcontent-ref %}
