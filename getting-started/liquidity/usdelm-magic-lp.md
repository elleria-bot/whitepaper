# $ELM-MAGIC LP

## Why Stake?

The v1 ELM-MAGIC Staking Pool emits a fixed amount of 500 $ELM a day, serving to kick start our LP by encouraging people to provide liquidity.

The staking pool is highly enticing, especially during the early phases of the project when the initial total supply is lower. There is no time lock on the ELM-MAGIC staking pool. Instead, a fee is incurred on LP Token withdrawals made within a short period.

<table><thead><tr><th width="233">Time Frame</th><th>Fees Incurred</th></tr></thead><tbody><tr><td>&#x3C; 1 Minute</td><td>50%</td></tr><tr><td>&#x3C; 1 Hour</td><td>20%</td></tr><tr><td>&#x3C; 1 Day</td><td>10%</td></tr><tr><td>&#x3C; 3 Days</td><td>5%</td></tr><tr><td>&#x3C; 7 Days</td><td>3%</td></tr><tr><td>&#x3C; 30 Days</td><td>1%</td></tr><tr><td>> 30 Days</td><td>0.5%</td></tr></tbody></table>

{% hint style="info" %}
Do note that any additional deposits will reset the timer for the whole balance.
{% endhint %}

In addition to emissions, there will be a leaderboard for this pool, where top stakers will have their combat attributes boosted.

| Leaderboard Ranking | Stats Boost |
| ------------------- | ----------- |
| Top 3%              | 10%         |
| Top 10%             | 8%          |
| Top 30%             | 5%          |
| Top 60%             | 2%          |
| Top 90%             | 1%          |

## How to Stake

<figure><img src="../../.gitbook/assets/image (3) (1) (1) (2).png" alt=""><figcaption></figcaption></figure>

1. Head to the Bank and click on "Staking"
2. Select "MagicSwap LP Staking"
3. Click on Deposit and enter the amount to deposit, then execute the transaction.

<figure><img src="../../.gitbook/assets/image (1) (4) (1).png" alt=""><figcaption><p>Check that your balance shows the correct amount of tokens staked</p></figcaption></figure>
