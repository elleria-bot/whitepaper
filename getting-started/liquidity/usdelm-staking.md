# \[Archived] $ELM Staking

{% hint style="danger" %}
Single-Sided $ELM Staking feature has been sunset and is no longer active.
{% endhint %}

## Why Stake?

The ELM staking pool exists for several purposes:

1. As a motivator for the re-investment of $ELM through passive rewards.
2. To bring the community together through global incentives and a common goal.
3. To reward in-game players through individual incentives.

The v1 ELM Staking Pool emits 250 $ELM daily. You staked $ELM is also affected by the amount of $ELM staked / the amount of off-chain circulating $ELM, creating a virtuous cycle where users lock $ELM for more rewards rather than selling them off.

| % off-chain $ELM staked | % emissions multiplier |
| ----------------------- | ---------------------- |
| below 30%               | 0%                     |
| up to 40%               | 50%                    |
| up to 50%               | 60%                    |
| up to 60%               | 80%                    |
| above 60%               | 100%                   |

**Your entire deposited balance will be locked for 4 weeks (28 days) upon a new stake.**

You can view all your staked $ELM and their details in our Bank. Upon the end of a staking period, you can choose to re-stake the deposited amount or withdraw it.

{% hint style="info" %}
After 4 weeks, you must re-deposit $ELM (i.e 0.01 $ELM) to re-lock the balance to continue earning emissions and boosts.
{% endhint %}

Emissions can be claimed at any time.

## How to Stake

<figure><img src="../../.gitbook/assets/image (25) (1).png" alt=""><figcaption></figcaption></figure>

1. Head to the Bank and click on "Staking"
2. Select "ELM Single-Sided Staking"
3.  Click on Deposit and enter the amount to deposit, then execute the transaction.\\

    <figure><img src="../../.gitbook/assets/image (26) (1).png" alt=""><figcaption><p>Check that your balance shows the correct amount of tokens staked.</p></figcaption></figure>
