# (Archived) Genesis Minting

**Mint Price:** 0.05 ETH\
**Mint Supply:** 9 000 Genesis Heroes\
**Whitelist Mints:** 2\
**Whitelist Mint Date:** 30th March 2022, 11:00 am GMT+8, 24 hours\
**Public Sales**: 31st March 2022, 11:00 am GMT + 8

**Before diving straight into launch, we would like to explain why we chose to have a paid mint approach.**

1. **Expansion**\
   We have plans to develop an entire working open-world Elleria for a fully immersive experience and proper real-time skill-based gameplay. With the capital raised from the paid mint, we will be hiring more 3D artists and developers to turn that vision into a reality. Please refer to our roadmap in the whitepaper for more details.
2. **Sunk costs**\
   If you’ve seen the sneak peeks and 3D art assets displayed on Twitter, you must know that it isn’t easy or cheap to develop. To date, the team has been working full-time for months solely on passion while consistently delivering. The team has also fronted other developmental costs such as server hosting, web hosting, marketing and will be fronting smart contract deployment costs.

> Did you know: The budget for free-to-play smash hit Genshin Impact was reportedly USD 100 million?

**Funding Allocation**

Capital raised through mint will firstly offset costs incurred by the team. After which, the remaining fund allocation is as follows:

**40%** will be used immediately to hire new full-timers and expand the team.\
**45%** will be the project’s treasury funds to be reallocated to the project.\
**15%** will be allocated for marketing and publicity.

**Minting Perks**

Genesis heroes have a minimum amount of stats allocated and will naturally be **stronger** than heroes minted post-launch. In Elleria, having higher base stats will be beneficial as heroes can complete more challenging quests that emit better rewards.

**Mintable Supply**

To better align the long-term interests of TreasureDAO and Tales of Elleria, we will be providing TreasureDAO with a commensurate value of NFTs & native tokens in return for $MAGIC grants. Thus, the total mintable supply will alter as follows\*\*:\*\*

Total Genesis Heroes: 10,000

| Allocation                       | Wallet Address                             | Quantity |
| -------------------------------- | ------------------------------------------ | -------- |
| TreasureDAO Vault (TreasureSwap) | 0x0eB5B03c0303f2F47cD81d7BE4275AF8Ed347576 | 500      |
| Team and Mods                    | 0x84c8Bd99Df40626f6d9cb9a1E71d2F65278D75fA | 200      |
| Marketing & Giveaways            | 0x84c8Bd99Df40626f6d9cb9a1E71d2F65278D75fA | 100      |
| Treasury Vault/ Reserves         | 0x84c8Bd99Df40626f6d9cb9a1E71d2F65278D75fA | 200      |
| Remaining Mintable Supply        | N/A                                        | 9000     |

You can view more detailed information [here](https://talesofelleria.notion.site/Genesis-Heroes-Allocations-656b0d5c6e364bfa9f04d2edc4d530db).

**Minting Out**

{% hint style="info" %}
**We have minted out on 11th October 2022! Thank you to those who have supported us, and onwards we go forth together!**
{% endhint %}

Regardless of whether the game mints out, the development will carry on. Minting will remain open until all Genesis heroes have been minted. The team sees this game as a long term commitment and will continue to develop so long as players are interested.

The **stats** of your heroes are the **main driving force** to how much in-game rewards you can make. Genesis heroes will be the cornerstone of earning in-game rewards since they offer higher stats and attributes.
