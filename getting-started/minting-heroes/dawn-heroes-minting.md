# \[Paid] Ninjas Summoning

{% hint style="danger" %}
Mint only from the Summoning Altar in [https://app.talesofelleria.com/](https://app.talesofelleria.com/). \
Official Mint CA: `0xF9283898176333f00E9041fc47fa79c74335E557`

Please do not fall for scams!
{% endhint %}

The last of the Dawn Heroes is finally here! Emerging from a shadowy realm, the Ninjas are here to slay. Just in time as our [10th World Boss](../../gameplay-prologue/world-boss-defi-kingdom-bloater-season-10.md) invades Elleria!

**Mint Date:** 18th December 2023 4AM UTC until sold out.\
**Mint Price:** 75 $MAGIC on Arbitrum One.\
**Supply:** 1000 Ninjas, no limits per wallet, no whitelist.

<figure><img src="../../.gitbook/assets/image.png" alt=""><figcaption><p>Summoning UI</p></figcaption></figure>

### **INCENTIVES FOR MINTING 💰** <a href="#cea1" id="cea1"></a>

1. Minting a Ninja will also automatically give **1x World Boss Ticket, 1x Timeless Memories (50 Hero EXP), 5x Diluted Recovery Potions, and 20,000 $MEDALS** to help players get started and compete in the World Boss!
2. Minting at least 1 Ninja on an account will give that account the 'Unsheathed Blade' boost. **All heroes within that account will gain +20% Combat Stats against the Bloater!** This boost expires on 1st January 2024. (+20% Base HP, F. ATK, and F. DEF)
3. Chance to mint CS heroes/strong Questoors/Legendary Heroes at low prices. These heroes go for a premium on the Marketplace, and **could** be more cost-efficient to mint! RNGeesus 🍀
4. Ninjas can earn $ELM, $MEDALS, and Relics from quests!&#x20;
5. Earn $ELM every biweekly from the [Tower of Babel](../../gameplay-prologue/game-features/tower-of-babel.md) leaderboards! A fresh slate means players can compete for the top spot with less interference!

{% hint style="info" %}
Ninjas have a minimum of 10 points per stat, similar to other Dawn Heroes. With high Agility, Vitality, and Strength, the Ninja is a great front-liner and is suited for crit, damage, or survivability builds depending on your needs.
{% endhint %}

**Ninja Stats**

Evildoers beware, the night is watching. The nimble Ninja blends into darkness and shadows to get where he needs to be, and his trusty katana slices all who dare to stand in his way. The ninja slices through enemies dealing **physical damage.**

100 Stat: Agility (Main Stat)\
90 Stat: Vitality (Sub Stat)\
75 Stat: Strength\
60 Stat: Will/Endurance\
50 Stat: Intelligence

<figure><img src="../../.gitbook/assets/image (5) (1) (1).png" alt=""><figcaption></figcaption></figure>

### Ninja Summoning Mechanism

You will need these ERC20 tokens on Arbitrum One:

**$MAGIC** (0x539bdE0d7Dbd336b79148AA742883198BBF60342)

Minting costs 75 $MAGIC per hero, and you will receive your hero on-chain immediately.

### How to Mint <a href="#cf54" id="cf54"></a>

1. Head to our game site: [https://app.talesofelleria.com/](https://app.talesofelleria.com/).
2. Connect your wallet to our site via Metamask. (Note: WalletConnect is temporarily bugged out)
3. Head to the **Summoning Altar** via the Events Page, or the World Map**.**

<figure><img src="../../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

4\. Click on **Summon Dawn Heroes.** In the UI that appears, you can choose the number of heroes to mint. After clicking 'Summon', you will be prompted to approve $MAGIC for our mint contract and to Mint.

<figure><img src="../../.gitbook/assets/image (4).png" alt=""><figcaption></figcaption></figure>

5. **Congratulations**! After minting, you will need to bind your hero's soul into the game to perform GASLESS in-game actions. You can bind/release your heroes at any time. (Manage NFTs > Heroes > Bind Soul)

<figure><img src="../../.gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

6. You are now ready to go! Mint your Ethereal Hero as well if you haven't, upgrade your heroes, and get ready to whack the Bloater and other monsters!

Refer [here ](../../welcome-to-elleria/guides/)for Elleria's in-game guides. \
If you're stuck or confused anytime, just reach out in our [Discord](https://discord.gg/talesofelleria)!&#x20;

Good luck with your mints everyone! 🍀
