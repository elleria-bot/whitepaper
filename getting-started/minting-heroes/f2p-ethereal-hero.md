# (FREE) Ethereal Hero

Ethereal Heroes are uniquely soul-bound (untradable) heroes. They act as a gateway for those wanting to “try” the game before fully immersing themselves in the world of Elleria.

You can mint your first Ethereal Hero for **FREE!**

To read everything about Ethereal Hero, refer to the link below:

{% content-ref url="../../gameplay-prologue/heroes/ethereal-heroes.md" %}
[ethereal-heroes.md](../../gameplay-prologue/heroes/ethereal-heroes.md)
{% endcontent-ref %}

