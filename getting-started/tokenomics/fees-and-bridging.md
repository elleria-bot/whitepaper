# Fees & Bridging

## Fees

**Transaction Fees:** We do not have any transaction fees on our tokens and NFTs apart from gas fees needed to complete transactions.

**Bridging Fees:** All emissions obtained in-game only exists off-chain and will not be minted until a user withdraws it. There are no fees for withdrawals and deposits too. Please refer to Bridging Mechanics.

**Minting Fees:** Minting is the team's primary revenue stream, together with marketplace transaction fees. Our team is dedicated to the project and will never act against the interest of our tokens. We have also set up our minting feature to take in $ETH, which users can take out without affecting our token prices. Furthermore, this will allow us to reserve tokens that we can use for buybacks and further development. All $ELM used in minting are burnt.

**Marketplace Fees:** We are charging a small fee for every transaction on Treasure Marketplace.

| Fees Allocation % | Purpose           | Addresses          |
| ----------------- | ----------------- | ------------------ |
| 70%               | Development Funds | Development Wallet |
| 30%               | Team Revenue      | -                  |

## Bridging Mechanics

Our off-chain server allows players to have an immersive game experience without paying excessive gas fees. Our mechanics keep the project sustainable and beneficial for players. As such, bridging is required for a player to use assets in the game.

All ERC721 and ERC1155 NFTs such as Heroes, Equipment, and Drops/Relics can be bridged and retrieved from the game at any time. To trade these NFTs, users must withdraw them from the game (transferred back to your Metamask Wallet).

All ERC20 Tokens, such as $ELLERIUM and $MEDALS, can also be bridged and retrieved from the game at any time. For $MEDALS ONLY, there is a claim tax upon withdrawal. Tokens bridged into the game are locked in the bridging conract that acts as the treasury.

Upon withdrawal, $MEDALS are minted. For $ELLERIUM, tokens will be withdrawn from the rewards pool. This rewards pool will be constantly refilled according to a release schedule until our hard cap is reached.

Our project has a built-in self regulating economy using our dual tokens and NFTs. We have designed our sinks to balance our our faucets, however, should there be insufficient tokens or when the total supply cap is reached (after 5 years), the ecosystem will shift towards using NFTs as value sinks as opposed to using tokens.
