# Sustainable Liquidity

## The Importance of Liquidity

High liquidity for our tokens is ideal. It ensures that prices are stable and not prone to large swings resulting from large trades. Doing so allows for ease of trading on DEXs. As such, our metaverse is built on a solid foundation, permitting long term continuance of the project.

We foresee growth of our project from our team's internal developments and from publicly-owned projects that will create a positive reinforcement loop over at least half a decade. We expect our tokens to be fully minted out at around the same time. We will withdraw the locked tokens from the treasury to restart the cycle. As such, we have planned for liquidity in a way that will build long-term sustainability.

Holders can stake both $ELM and $MEDALS liquidity through SushiSwap for LP tokens.

{% hint style="info" %}
**Note:** Do take note that there is Impermanent Loss (IL) if the value of the tokens in the pair changes relative to one another. You can learn more about this by [watching this video](https://www.youtube.com/watch?v=8XJ1MSTEuU0\&ab\_channel=Finematics).
{% endhint %}

## Incentives for providing Liquidity

#### $ELM-MAGIC LP Staking Pool

$ELM is the main currency in our metaverse. Users are able to stake their ELM-MAGIC LP tokens in our staking pool for $ELM emissions. This allows the project to maintain a healthy token supply growth without requiring users to play the game.

#### Expected APR (Initial)

With an initial liquidity pool size of 10,000 $ELM tokens, the LP mine emits ELM at a rate of 2.5% a day. Token emissions are as follows:

* **250 $ELM** a day
* **7500 $ELM** a month

{% hint style="info" %}
The LP mine has been boosted to 500 $ELM a day to incentivize LP staking.
{% endhint %}

## Gamified Incentives for Liquidity

### **Individual Incentives for LP Staking**

**Liquidity Pool x In-Game Buffs**

As the project matures, players seeking to progress further are provided with alternative methods to strengthen their characters and teams. We are looking into the potential of embedding the usage of LP tokens into specific game mechanics. For example:

* Users staking LP-tokens into particular pools are rewarded with buffs.
* Users are required to wrap and use LP-tokens to claim houses and lands or for specific game mechanics such as excavating relics or creating guilds.

### **Community Incentives for LP Staking**

**Liquidity Pool x World Expansion**

There is also potential where the size of the LP can affect the state of the games, for example, in unlocking new regions and expanding the world.

* Once the game expands to a particular state where players conquer the city's outskirts, the community is able to reclaim lands that serve as outposts for further adventuring. These lands will require LP to sustain the trade activities and operations within.

**Liquidity Pool x Combat Mechanics**

There are also plans for community-driven LP activities in wars and raids, where the size of the LP determines the enemy's strength, rewards, and difficulty.

* Once lands are unlocked, we have a framework for different land-related mechanics such as wars, guilds, city-based weapons & defences. Users will collectively upgrade land using LP tokens to fortify these outposts further and reduce the difficulty of wars and raids.

Once these outposts have been fully fortified, the citizens of Elleria can then adventure outwards to conquer more lands once again. With them managing the city, players can then withdraw their LP tokens and move them into the next place needed for conquering.

**Our Goal: Community-Driven Sustainable LP Mechanics**

This way, the project becomes community-driven. The direction of the world's expansion and failure/success rates is determined by all the players collectively. Everybody will work and be rewarded collectively, allowing the community to find a common goal and interest to encourage bonding and positive interactions.
