# Treasury Allocation

This page thoroughly explains the usage of all capital raised from mint and the exact fund allocation to provide greater transparency. As one of the first paid mint projects in the Arbitrum Ecosystem, we want to set the standard of how transparent preceding teams should be with their treasury allocations.

### Allocations

<table><thead><tr><th width="150">Percentage</th><th>Allocation</th></tr></thead><tbody><tr><td>60%</td><td>Allocated to Treasury</td></tr><tr><td>20%</td><td>Allocated to the recovery of existing developmental costs</td></tr><tr><td>20%</td><td>Allocated to the team as profits</td></tr></tbody></table>

### Treasury Allocation

Operational costs for developing the project will be vested linearly over a 2-year timeline from the treasury to ensure a decently size runway to deliver what we intend to as stated in the roadmap.

With the newly acquired funds, we will be looking to on-board three more developers and two more artists working full time on Tales of Elleria. This means faster game development and providing the community with what they want even more efficiently.

### Existing Developmental Costs

Creating a 3D game is by no means a low-cost feat. Several artists and developers worked overtime for months to achieve what we have today. We also included developmental software and hardware costs. We will be allocating a portion towards reimbursing these costs incurred.

### Team Profits

The team will be distributing 20% of overall mint capital among the team of 12. This is to reward them for their hard work and sleepless nights they have dedicated to the project.

These 20% will be vested equally over 3 epochs. Each epoch will last 3 months and the first withdrawal will be effective immediately upon mint out.\\

## Withdrawals

As the Tales of Elleria team functions similar to a Web 2.0 organization, the accrued developmental costs will be withdrawn to Fiat and reimbursed off-chain. The treasury of 60% of funds will be vested over 2 years on a monthly burn rate of USD 37,500 per month to cover all operating costs for a 12 member team. The remaining 20% of funds will be vested equally over 3 epochs and distributed to the team upon mint out.

In the short-term, Tales of Elleria will be withdrawing 90 ETH (costs).

With the current development plan laid out, we hope that our players will stick around and enjoy the coming years with us.
