# $ELLERIUM

## Ellerium ERC20 Token

CA: 0xEEac5E75216571773C0064b3B591A86253791DB6

![](../../.gitbook/assets/ElleriumBunch.png)

**ELLERIUM** is a rare natural resource found within the world of Elleria, said to be the crystallized blessing of the Goddess, and is used as the main currency throughout the whole metaverse.

$ELM can be used to summon heroes. Within the game, $ELM is used to strengthen heroes and create blessed equipment, allowing for player progression.

### Total Suppl**y: 2,500,000 $ELM**

<table data-header-hidden><thead><tr><th width="214.33333333333331">Holder</th><th width="150">ELM (%)</th><th width="150">ELM ($)</th><th>Vesting</th></tr></thead><tbody><tr><td></td><td>ELM (%)</td><td>ELM ($)</td><td>Vesting</td></tr><tr><td>Ecosystem</td><td>69%</td><td>1,725,000</td><td>Vested as game progresses</td></tr><tr><td>TreasureDAO (Seed)</td><td>5%</td><td>125,000</td><td>6 month cliff; vested linearly over 5 years</td></tr><tr><td>Team</td><td>20%</td><td>500,000</td><td>9 month cliff; vested linearly over 5 years</td></tr><tr><td>Liquidity</td><td>6%</td><td>150,000</td><td>N/A</td></tr></tbody></table>

{% hint style="info" %}
Vests can be checked through: [https://locker.talesofelleria.com/](https://locker.talesofelleria.com/)
{% endhint %}

### Ellerium Utility

Ellerium is considered a more "premium" currency used to perform important 'creation‘ and high-tier actions. Ellereum can also be staked in the Ellerian Bank for Single Sided Staking Boosts & Yields & in-game buffs.\
\
Ellerium in game utility includes (but not limited to):

* Use $ELM to Mint Heroes.
* Use $ELM to Mint Equipment.
* Use $ELM to Craft Items.
* Use $ELM to Upgrade Heroes (high level).
* Use $ELM to build & upgrade Houses.
* Use $ELM to reclaim & upgrade Land.

### Tokenomic Schedule

<figure><img src="../../.gitbook/assets/image (8) (2).png" alt=""><figcaption><p>$ELM Emissions Schedule</p></figcaption></figure>

The total supply for $ELM is 2,500,000.

* 69% of all tokens are emitted through the game
* 6% has been seeded in the LP
* 20% goes to the team
* 5% goes to TreasureDAO

According to the Tokenomic emission schedule, a “halvening” will be introduced every 12 months from the ecosystem emissions so as to prevent drastic price changes in the longevity of the game, as well as reward early players. In addition, as the cliff for the team and TreasureDAO of 6 months have been reached, both parties will also be vested ELM tokens linearly across the span of 5 years.

In the event where the burnt supply grows to a substantial portion of the overall supply, a recycling ratio or bonus multiplier will be introduced to emit more $ELM in the ecosystem. More on that later on when the new Tokenomic schedule has stabilised. Please note that there is not max supply on the contract itself, and the emission schedule is a hard guideline the team will follow to periodically balance $ELM towards the schedule.

{% hint style="info" %}
After the $ELM exploit, the team allocated an additional 7% of the $ELM supply from Elleria's Treasury (burnt $ELM) as part of a treasury swap agreement with TreasureDAO in exchange for $MAGIC to reseed the lost Liquidity Pool.
{% endhint %}

Please note that the tokenomics are still a work in progress subject to change.
