# World Boss: A Rift in Reality ft. Smolverse (Season 2)

![Zorag, the Goblin King](<../../.gitbook/assets/goblin\_camp\_ex\_withking (2).png>)

A great rift tore through the sky, enveloping the City of Elleria. Through this rift came a huge monster that started wreaking havoc all over the city.

Challenge Zorag and save Elleria! This time, the smols are here to help!

## Entrance Ticket

![](<../../.gitbook/assets/image (20) (1).png>)

To enter the boss dungeon, you must have one entrance ticket. These tickets can be crafted from item drops attained from quests or purchased from the adventurer's shop.\
\
Please refer to the [recipe list](../../gameplay-prologue/crafting-system/crafting.md#consumable-recipes) for the recipe to craft this ticket.

## World Boss Structure

In this boss fight, heroes will attempt to deal as much damage as possible before perishing to the world boss. Rewards will be allocated according to ranking.

A player can send an unlimited number of heroes into the world boss so long as he has sufficient entry tickets. Please note that quest attempts are shared with normal quests in the Goblin Plains.

\
**The highest damage attempt per hero, accumulated across all heroes, will be ranked on the Leaderboard.**

Example:\
You have two heroes and send both into the world boss multiple times. Your best attempt for hero A is 5,000 damage done, and your best attempt for hero B is 2,000 damage done.

Your total damage on the leaderboard is 7,000.

\\

## World Boss Mechanics

* There'll be a set pattern for the first ten turns but subsequently randomized between normal & special attack at a specific chance.
* Zorag's normal attack will have a minimum 25% chance of hitting a player, while his special attacks have increased accuracy.
* The longer the battle, the more invigorated Zorag will be. Beyond ten turns, Zorag enters into rage mode and will have progressively increasing stats.
* You can use one potion per turn, the same as in normal quests.
* Any damage immediately adds towards the total damage dealt, and you do not need to wait for the hero to be defeated.
* Mages deal an increased 15% damage to the world boss.
* Assassins deal an increased 20% damage to the world boss.
* Zorag's stats will remain the same and will not be randomized

<table><thead><tr><th width="233.05650952232276">Stats</th><th>Amount</th></tr></thead><tbody><tr><td>Strength</td><td>60</td></tr><tr><td>Agility</td><td>40</td></tr><tr><td>Endurance</td><td>50</td></tr><tr><td>Will</td><td>60</td></tr></tbody></table>

## Smolverse Integration

![](<../../.gitbook/assets/image (17) (3).png>)

As a special thanks to the TreasureDAO team and smolverse for everything they have done for us and for contributing the biggest portion of the World Boss prize pool, we’ve decided to provide utility to smol holders first.

Holding at least 1 smolbrain NFT gives you +20% Attack\
Holding at least 1 smolbody NFT gives you +20% Defence\
Holding at least 1 smolbrains land NFT gives you +10% Combat Agility

May all smol holders rank high on the Leaderboard!

## $MAGIC Rewards

Distributed $MAGIC will come from entry tickets and the Treasure emission grants (TIP 09).

### Reward pool: TIP09 $MAGIC + Percentage of Entrance Fees

$MAGIC Distribution: Allocated across the damage leaderboard

![World Boss Rankings](<../../.gitbook/assets/image (17) (2).png>)

**1st Place**: 5% of the pool\
**2nd Place**: 4% of the pool\
**3rd Place**: 3% of the pool\
**4–10th Place**: 2% of the pool\
**11–25th Place**: 1% of the pool\
**26–50th Place**: 0.75% of the pool\
**51–100th Place**: 0.6% of the pool\
**101–200th Place**: 0.1% of the pool\
Participation reward: One random relic awarded after bi-weekly first clear.\
Do at least 1 damage to count towards the reward pool!

Rewards will be distributed after the event is over to all players at once. Players will not receive any rewards immediately from the quest.
