# World Boss: A Rift in Reality (Season 3)

![Zorag, the Goblin King](<../../.gitbook/assets/goblin\_camp\_ex\_withking (2).png>)

Zorag has exhausted his forces and strength, and has almost been defeated. This time, Ellerians need only send 1 hero in at a time- the strongest, mind you, to exhaust him and push him back!

## Entrance Ticket

![](<../../.gitbook/assets/image (20) (1).png>)

To enter the boss dungeon, you must have one entrance ticket. These tickets can be crafted from item drops attained from quests or purchased from the adventurer's shop.\
\
Please refer to the [recipe list](../../gameplay-prologue/crafting-system/crafting.md#consumable-recipes) for the recipe to craft this ticket.

## World Boss Structure

This season, players will be ranked according to the **highest cumulative damage done across all attempts for their best hero.**

E.G. You have three heroes:\
\- Hero 1 does 10,000 damage across 5 attempts\
\- Hero 2 does 2,000 damage across 3 attempts\
\- Hero 3 does 5,000 damage across 1 attempt

Your rank on the Leaderboard will be 10,000 damage done (Hero 1 has the most damage).

\
**The hero with the highest damage irregardless of attempt will be ranked on the Leaderboard.**

## World Boss Mechanics

* There'll be a set pattern for the first ten turns but subsequently randomized between normal & special attack at a specific chance.
* Zorag's normal attack will have a minimum 25% chance of hitting a player, while his special attacks have increased accuracy.
* The longer the battle, the more invigorated Zorag will be. Beyond ten turns, Zorag enters into rage mode and will have progressively increasing stats.
* You can use one potion per turn, the same as in normal quests.
* Any damage immediately adds towards the total damage dealt, and you do not need to wait for the hero to be defeated.
* Mages deal an increased 15% damage to the world boss.
* Assassins deal an increased 20% damage to the world boss.
* Zorag's stats will remain the same and will not be randomized

<table><thead><tr><th width="233.05650952232276">Stats</th><th>Amount</th></tr></thead><tbody><tr><td>Strength</td><td>60</td></tr><tr><td>Agility</td><td>40</td></tr><tr><td>Endurance</td><td>50</td></tr><tr><td>Will</td><td>40</td></tr></tbody></table>

## Leaderboard Updates

This season of World Boss will begin on 22 August, 12 AM EST and will conclude on 4 September, 11:59 PM EST.

We’ve also made **huge improvements** to the Leaderboard UI, where you can now inspect the top heroes, the total $MAGIC prize pool, as well as time remaining!

![Updated Leaderboard UI](https://miro.medium.com/max/1400/1\*bTyYFd2GsIY1sydxFGpFLw.jpeg)

## $MAGIC Rewards

Distributed $MAGIC will come from entry tickets and the Treasure emission grants (TIP 09).

### Reward pool: TIP09 $MAGIC + Percentage of Entrance Fees

$MAGIC Distribution: Allocated across the damage leaderboard

![World Boss Rankings](<../../.gitbook/assets/image (17) (2).png>)

**1st Place**: 5% of the pool\
**2nd Place**: 4% of the pool\
**3rd Place**: 3% of the pool\
**4–10th Place**: 2% of the pool\
**11–25th Place**: 1% of the pool\
**26–50th Place**: 0.75% of the pool\
**51–100th Place**: 0.6% of the pool\
**101–200th Place**: 0.1% of the pool\
Participation reward: One random relic awarded after bi-weekly first clear.\
Do at least 1 damage to count towards the reward pool!

Rewards will be distributed after the event is over to all players at once. Players will not receive any rewards immediately from the quest.
