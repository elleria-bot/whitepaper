# World Boss: Bloborg the Slime Overlord (Season 7 - Dawn Heroes Only)

<figure><img src="../../.gitbook/assets/image (11) (2).png" alt=""><figcaption><p>Bloborg, the Slime Overlord</p></figcaption></figure>

_EMERGENCY ALERT!_

_Attention all citizens of Elleria! The Slime Overlord and his army of slimes are attacking our city as we speak. We need every able-bodied person to come forward and defend our home._

_Do not be afraid, for we are strong and united. We must stand together to protect our families, our friends, and our way of life. Grab your weapons, gather your courage, and join the fight._

_Please report to the town square immediately. The safety of our town is at stake. Let us show the Slime Overlord what we’re made of. Together, we will overcome this challenge and emerge stronger than ever before._

_For Elleria._

**World boss is back with its 7th Season!**

This time, Bloborg the Slime Overlord is here to terrorize Ellerians! This Season, not only did Bloborg bring his slime army, but he also brought plentiful relics and loot. Defeat him to earn bountiful treasure!

## Entrance Ticket

![](<../../.gitbook/assets/image (20) (1).png>)

To enter the boss dungeon, you must have one entrance ticket. These tickets can be crafted from item drops attained from quests or purchased from the adventurer's shop.

\
\
Please refer to the [recipe list](../../gameplay-prologue/crafting-system/crafting.md#consumable-recipes) for the a full recipe list. The recipe for crafting entry ticket is as shown:

<figure><img src="../../.gitbook/assets/image (5) (1) (2).png" alt=""><figcaption><p>Buy tickets from the shop in Adventurer's Guild! Please dont leave the page until the transaction completes.</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (2) (2).png" alt=""><figcaption><p>Craft tickets for a cheaper entry!</p></figcaption></figure>

## World Boss Structure

This season, players will be ranked according to the **highest cumulative damage done across all attempts for all heroes.**

E.G. You have three heroes:\
\- Hero 1 does 10,000 damage across 5 attempts\
\- Hero 2 does 2,000 damage across 3 attempts\
\- Hero 3 does 5,000 damage across 1 attempt

Your rank on the Leaderboard will be 17,000 damage done (Total damage across all heroes and attempts).

## World Boss Mechanics

* There'll be a set pattern for the first ten turns but subsequently randomized between normal & special attacks at a specific chance.
* Bloborg’s normal attack will have a minimum 25% chance of hitting a player, while his special attacks have increased accuracy.
* The longer the battle, the more invigorated Bloborg will be. Beyond ten turns, Bloborg enters into rage mode and will have progressively increasing stats.
* You can use one consumable per turn, the same as in regular quests.
* Any damage immediately adds towards the total damage dealt, and you do not need to wait for the hero to be defeated. However, the leaderboards will only update at 15-minute intervals.
* Bloborg’s stats will remain the same throughout the event period.

<table><thead><tr><th width="233.05650952232276">Stats</th><th>Amount</th></tr></thead><tbody><tr><td>Intelligence</td><td>40</td></tr><tr><td>Agility</td><td>35</td></tr><tr><td>Endurance</td><td>20</td></tr><tr><td>Will</td><td>50</td></tr></tbody></table>

## $MAGIC Rewards

Distributed $MAGIC will come from entry tickets and the Treasure emission grants (TIP 09).

### Reward pool: TIP09 $MAGIC + Percentage of Entrance Fees

<figure><img src="../../.gitbook/assets/image (3) (2) (2).png" alt=""><figcaption><p>You can see $MAGIC pool, time left, distribution, damage, from the leaderboards.</p></figcaption></figure>

This season, the starting reward pool is **8,888 $MAGIC**.

**1st Place:** 5% of the pool\
**2nd Place:** 4% of the pool\
**3rd Place:** 3% of the pool\
**4–10th Place:** 2% of the pool\
**11–25th Place:** 1.5% of the pool\
**26–50th Place:** 1% of the pool\
**51–100th Place:** 0.3% of the pool\
**101–200th Place:** 0.1% of the pool\
**Special: The player who sends the most number of heroes gets 1.5% of the pool**\
**Participation Reward:** 5,000 MEDALS\\

Rewards will be distributed after the event is over to all players at once. Players will not receive any rewards immediately from the quest.

## Additional Notes <a href="#d170" id="d170"></a>

* This Season, we’ve introduced world boss only for Dawn heroes for two main reasons, due to balancing of characters and providing more utility to dawn heroes to drive mints. The prize pool of $MAGIC provides incentives to players who continually support the project.
* $ELM is now supported on CoinGecko and CoinMarketCap! Check out the links below to add them to your watchlist.
* We’re looking to hire a full-time marketing, collaborations and sales representative. Please feel free to reach out to Smashe for more information.
* We are currently open to suggestions for feedback about the upcoming MagicSwap v2 following TreasureDAO’s announcement. Please visit Discord to give feedback or learn more about how this affects Elleria.
* As always, Elleria is open to suggestions about improving the state of the game. Do leave suggestions in Discord on how we can grow as a team and community.

Official Social Media\
[Play Game ](https://app.talesofelleria.com/)| [Whitepaper](https://docs.talesofelleria.com/) | [Marketplace ](https://marketplace.treasure.lol/collection/tales-of-elleria)|[ ](https://gunstar.io/)[Twitter](https://twitter.com/TalesofElleria) | [Discord](https://discord.gg/TalesofElleria) | [Website](https://www.talesofelleria.com/) | [Medium](https://medium.com/@talesofelleria/) | [Buy $ELM](https://magicswap.lol/) | [$ELM Chart](https://www.defined.fi/arb/0xf904469497e6a179a9d47a7b468e4be42ec56e65?cache=41f8e) | [Coin Gecko](https://www.coingecko.com/en/coins/ellerium) | [Coin Market Cap](https://coinmarketcap.com/currencies/ellerium/)
