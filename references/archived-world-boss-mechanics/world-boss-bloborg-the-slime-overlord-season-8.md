# World Boss: Bloborg the Slime Overlord (Season 8)

<figure><img src="../../.gitbook/assets/image (11) (2).png" alt=""><figcaption><p>Bloborg, the Slime Overlord</p></figcaption></figure>

_EMERGENCY ALERT! BLOBORG IS BACK!_

_Attention all citizens of Elleria! The Slime Overlord and his army of slimes are attacking our city as we speak. We need every able-bodied person to come forward and defend our home._

_Do not be afraid, for we are strong and united. We must stand together to protect our families, our friends, and our way of life. Grab your weapons, gather your courage, and join the fight._

_Please report to the town square immediately. The safety of our town is at stake. Let us show the Slime Overlord what we’re made of. Together, we will overcome this challenge and emerge stronger than ever before._

_Defeat Bloborg to be rewarded with plentiful relics, medals and loot!_

**World boss is back with its 8th Season!**

## Event Details <a href="#880d" id="880d"></a>

This Season, **ALL HEROES** are eligible to participate in fighting for the largest share of the prize pool, up to **5,600 $MAGIC.**

World Boss Season 8 begins on **13th March, 4.01 AM UTC** and concludes on **26th March, 3:59 AM UTC.**

## Entrance Ticket

![](<../../.gitbook/assets/image (20) (1).png>)

To enter the boss dungeon, you must have one entrance ticket. These tickets can be crafted from item drops attained from quests or purchased from the adventurer's shop.

<figure><img src="../../.gitbook/assets/image (5) (1) (2).png" alt=""><figcaption><p>Buy tickets from the shop in Adventurer's Guild! Please dont leave the page until the transaction completes.</p></figcaption></figure>

\
Please refer to the [recipe list](../../gameplay-prologue/crafting-system/crafting.md#consumable-recipes) for the full recipe list. The recipe for crafting an entry ticket is as shown:

<figure><img src="../../.gitbook/assets/image (2) (2).png" alt=""><figcaption><p>Craft tickets for a cheaper entry!</p></figcaption></figure>

## World Boss Structure (NEW) <a href="#9670" id="9670"></a>

In this boss fight, heroes will attempt to deal as much damage as possible before perishing to the world boss. Rewards will be allocated according to ranking.

Please note that quest attempts are shared with normal quests in the Goblin Plains. The **cumulative damage done across all heroes’ best attempts** will be ranked on the Leaderboard\*\*.\*\*

_E.G. You have three heroes:_\
_- Hero 1 does 10,000 damage on his 1st attempt_\
_- Hero 1 does 7,000 damage on his 2nd attempt_\
_- Hero 2 does 5,000 damage on his 1st attempt_

_Your rank on the Leaderboard will be 15,000 damage done._

## World Boss Mechanics

* There'll be a set pattern for the first ten turns but subsequently randomized between normal & special attacks at a specific chance.
* Bloborg’s normal attack will have a minimum 25% chance of hitting a player, while his special attacks have increased accuracy.
* The longer the battle, the more invigorated Bloborg will be. Beyond ten turns, Bloborg enters into rage mode and will have progressively increasing stats.
* You can use one consumable per turn, the same as in regular quests.
* Any damage immediately adds towards the total damage dealt, and you do not need to wait for the hero to be defeated. However, the leaderboards will only update at 15-minute intervals.
* Bloborg’s stats will remain the same throughout the event period.

<table><thead><tr><th width="233.05650952232276">Stats</th><th>Amount</th></tr></thead><tbody><tr><td>Intelligence</td><td>60</td></tr><tr><td>Agility</td><td>40</td></tr><tr><td>Endurance</td><td>50</td></tr><tr><td>Will</td><td>40</td></tr></tbody></table>

## $MAGIC Rewards

Distributed $MAGIC will come from entry tickets and the Treasure emission grants (TIP 09).

### Reward pool: TIP09 $MAGIC + Percentage of Entrance Fees

<figure><img src="../../.gitbook/assets/image (3) (2) (2).png" alt=""><figcaption><p>You can see $MAGIC pool, time left, distribution, damage, from the leaderboards.</p></figcaption></figure>

This season, the starting reward pool is **5,600 $MAGIC!**

**1st Place:** 5% of the pool\
**2nd Place:** 4% of the pool\
**3rd Place:** 3% of the pool\
**4–10th Place:** 2% of the pool\
**11–25th Place:** 1.5% of the pool\
**26–50th Place:** 1% of the pool\
**51–100th Place:** 0.3% of the pool\
**101–200th Place:** 0.1% of the pool\
**Special: The player with the most heroes sent gets 1.5% of the pool**\
**Participation Reward:** 5,000 MEDALS\\

Rewards will be distributed after the event is over to all players at once. Players will not receive any rewards immediately from the quest.

## Additional Notes <a href="#d170" id="d170"></a>

* We originally had a bigger world boss planned that was delayed due to some constraints. This world boss is an intermission leading to the bigger world boss event.
* Special attacks of Spiritualists will arrive during the **second week** of the world boss! The mechanics of world boss itself, however, will not be altered during the entire duration of the event.
* The Tales of Elleria Team has a surprise planned for all ToE x TreasureDAO Social Event attendees in Sydney this April! Do head down if you’re in the area.
* Spiritualist mints are ongoing. Please check out this medium article for more information: [https://medium.com/@talesofelleria/spiritualist-mint-56ffdf47e289](https://medium.com/@talesofelleria/spiritualist-mint-56ffdf47e289)

Fortuna Eterna\
\#ForElleria 🍀

Official Social Media\
[Play Game ](https://app.talesofelleria.com/)| [Whitepaper](https://docs.talesofelleria.com/) | [Marketplace ](https://marketplace.treasure.lol/collection/tales-of-elleria)|[ ](https://gunstar.io/)[Twitter](https://twitter.com/TalesofElleria) | [Discord](https://discord.gg/TalesofElleria) | [Website](https://www.talesofelleria.com/) | [Medium](https://medium.com/@talesofelleria/) | [Buy $ELM](https://magicswap.lol/) | [$ELM Chart](https://www.defined.fi/arb/0xf904469497e6a179a9d47a7b468e4be42ec56e65?cache=41f8e) | [Coin Gecko](https://www.coingecko.com/en/coins/ellerium) | [Coin Market Cap](https://coinmarketcap.com/currencies/ellerium/)
