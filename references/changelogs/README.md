---
description: (Whitepaper Changes)
---

# Changelogs

**Week 2 / December 2023 -**\
Added Ninja Mint Information\
Added Bloater World Boss Info\
Added Glossary

**Week 4 / November / 2023 -**\
Added Equipment System page\
Updated existing combat stats formula\
Updated existing Skills/Traits effects\
Updated existing UI images

**Week 4 / September / 2023 -**\
Added disclaimer to Tokenomics page\
\
**Week 4 / August / 2023 -**\
Updated phrasing for Assignments/Explorations page\
Added Progress Packs to Dimensional Haul\
Added Kevin World Boss\
\
**Week 2 / August / 2023 -**\
**Added Ethereal Hero Information**\
**Added Guides**\
**Updated Explorations Information after halvening**\
**Updated Gold Rush to indicate its in Specials.**

**Week 4 / July / 2023 -**\
Updated Quest Information\
Updated Soulbound Relics Information

**Week 2 / July / 2023 -**\
Updated Official Links\
Added Vial Recipes\
Updated GQL information\
Updated PDS and JT's info in Artifacts

**Week 1 / July / 2023 -**\
Added information to the Tower of Babel Feature\
Updated contract addresses for $ELM\
Updated consumables to include recovery vials\
\
**Week 4 / June / 2023 -**\
Updated EXP info when hero is transferred\
Updated $ELM Tokenomics section\
Removed outdated info from Heroes

**Week 2 / June / 2023**\
Added information to the Tower of Babel Feature\
Updated reward on the Tower of Babel Feature

**Week 2 / April / 2023 -**\
Updated Scrolls FAQ\
Added Tower of Babel Stub\
Updated TOS\
Updated Roadmap\
\
**Week 1 / April / 2023 -**\
Added Hero Rebirth Feature information\
Added Soulbound Attributes information\
Updated FAQ portion for scrolls\
Updated RelicIds for soulbound scrolls

**Week 3 / March / 2023 -**\
Added Spiritualist Special Attack Information\
Updated Task feature Information

**Week 2 / March / 2023 -**\
Added World Boss Season 8 Information

**Week 4 / February / 2023 -**\
Added Spiritualist's CS Assignment Info\
Updated Scroll Mechanics in Detail

**Week 3 / February / 2023 -**\
Added Berserker's CS Assignment Info\
\
**Week 2 / February / 2023 -**\
Added World Boss Season 7 Information\
Added Skill information for Dawn Heroes (subject to change)\
Added Elixir Balancing Notes

**Week 4 / December / 2022 -**\
Updated Core Gameplay Loop Diagram\
Add Region Key & Clover Info/Recipes\
Update IDs for new Slime Stuff\
Updated Minting Page\
Added Information for Wishing Well/Slime Road\
Added Section in 'Quests' regarding Permadeath\
Added Section in Heroes regarding inactive heroes\
\
**Week 3 / December / 2022 -**\
Updated EXP required in wihtepaper\
Added 2022 Summary Medium Article

**Week 2 / December / 2022 -**\
Added Machinist & Minting Information\
Added Dawn Heroes Information\
Updated Numbers According to Level Revamp

**Week 3 / November / 2022 -**\
Added Vesting Information

**Week 2 / November / 2022 -**\
Added Genesis Hero Allocation Logs

**Week 1 / November / 2022 -**\
Added Terms of Service\
Added Daily/Weekly Tasks\
Added Game Modules under Tech & Innovations\
Added Info on how to bind/unbind ELM and Relics

**Week 4 / October / 2022 -**\
Added Explorations\
Updated Shop Listings for Medals Exchange Ticket

**Week 1 / October / 2022 -**\
Added information into the Tokenomics section ($ELM)\
Updated Crafting system to ELM Shards only\
Updated all recipes\
Added Scrolls and Staking into GraphQL

**Week 2 / September / 2022 -**\
Added mention of daily task in quickstart\
Added FAQ for untradeable scrolls effect

**Week 4 / August 2022 -**\
Added ELM CA: 0xf904469497e6a179a9d47a7b468e4be42ec56e65\
Updated liquidity and staking section\
Updated world boss section\
\
**Week 3 / August / 2022 -**\
Updated Ellerium tokenomics

**Week 1 / August / 2022 -**\
Added GraphQL for crafting/scroll logs\
Added IDs for relics and crafting recipes

**Week 3 / July / 2022 -**\
Updated recipes to include scrolls\
Added world boss #2 page.\
\
**Week 2 / July / 2022 -**\
Updated EXP reset

**Week 4 / June / 2022 -**\
Added Shop page\
Updated recipes\
Added World Boss page

**Week 3 / June / 2022 -**\
Added users query to GraphQL\
Updated crafting recipes\
Added Quickstart page

**Week 2 / June / 2022 -**\
Updated crafting recipes

**Week 2 / June / 2022 -**\
Added Crafting System and recipes\
\
**Week 1 / June / 2022 -**\
Added relics reward to GraphQL

**Week 2 / June / 2022 -**\
Added Crafting System and recipes\
Added more information on consumables\
\
**Week 1 / June / 2022 -**\
Added relics reward to GraphQL

**Week 4 / May / 2022 -**\
Updated Relics/ Drops System\
\
**Week 3 / May / 2022 -**\
Updated EXP required to level up (reflecting correct values)\
Clarified 'lose upgrades' -> 'lose all experience' when unbinding

**Week 2 / May / 2022 -**\
Added 'Technology & Innovations'.\
Added Quests update for Outposts & Gold Rush

**Week 1 / May / 2022 -**\
Added GraphQL section with Examples, Snippets, Ids, and Mappings.

**Week 4 / April / 2022-**\
Updated tokenomics - $ELM and $MEDALS info\
Updated all relevant segments involving $ELM\
Moved assignment emissions multiplier from heroes tab to assignments tab

**Week 3 / April / 2022-**

Updated Guide on how to bridge (bind/release) heroes\
Updated skill effects. Assassin crit 220% to 150%; Ranger bleed from 3 turns to 2-4 turns; Mage special damage 20% to 70%, resistance down from 3 to 1-3 turns; Warrior shield bash from 80% to 110% damage, defense up from 2 to 1-3 turns.\
Updated damage range from 50-150% to 70-130%\
Added Goblin Plains page\
Added Quest Rewards segment\
Update Lore

**Week 2 / April / 2022 -**

Updated Safe Address & Info\
Updated Treasury Allocation\
Updated Assignment Names

**Week 4 / March / 2022 -**

Changed combat formulas from 20%/level to 50%/level\
Finalized Attributes from (0-300, 301-400, 401+) to (0-300, 301-375, 376+)\
Fixed terminology (vitality, will) in assignments

**Week 2 / March / 2022 -**\
Balanced game & assignment formulas;\
Edited some portions of the whitepaper to be SEC compliant;\
Edited to say public API is only ready after launch;\
Included link to repository for contract addresses;\
Added seconds conversion for assignments;
