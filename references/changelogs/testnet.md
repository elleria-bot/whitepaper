# Testnet

| 28 March - v1.0.7 Mobile Update                                    |
| ------------------------------------------------------------------ |
| - Login: Wallet address added back for clarity                     |
| - Music: If muted, music stays muted when refreshed                |
| - Contracts: Updated to new set of contracts to test for whitelist |
| - UX: Added wallet balances to the status bar at the bottom        |
| **- MOBILE: All UI customized for mobile!**                        |
| - UX: Made all scrollviews draggable!                              |
| **- ERC721 NES: Your heroes stay with you even when staking!**     |

| 25 March 2022 - v1.0.5 & v1.0.6                         |
| ------------------------------------------------------- |
| - Mobile: WalletConect Added                            |
| - Backend: database ported                              |
| - Claiming: $ELLERIUM and $MEDALS claiming both work    |
| - Quests; Button in Quests to select hero made clearer  |
| - Fixed status effects causing monster HP to go under 0 |
| - Green in Hero page is less sharp                      |
| - Register: Messages now clearer.                       |
| - Battle: Rewards text made clearer.                    |
| - Battle: EXP indicator shows 'MAXED" if appropriate    |
| - Quest: Entry fee is checked for before signing!       |
| - Overall: Messages made clearer                        |
| - Signing: All transactions now expose data             |
| - Upgrades: Feedback text made yellow                   |
| - Security: Signing works for ledger                    |

| 17 March 2022 - v1.0.4                                                                                                             |
| ---------------------------------------------------------------------------------------------------------------------------------- |
| - Summoning: Release Heroes now work                                                                                               |
| - Proper error message is shown when you claim exceeding your balance                                                              |
| - Proper error message is shown when you try to claim 0                                                                            |
| - Assignments: Upon claiming, hero salary is automatically refreshed.                                                              |
| - Summoning: When binding and releasing, there is a delay and your heroes will be updated automatically if there is no congestion. |
| - Summoning: Error prompts appears if you didn't select heroes                                                                     |
| - Profile Creation: Error prompt appears if invalid name is entered.                                                               |
| - Text: NIL has been changed to more appropriate terms.                                                                            |
| - Renaming: Fee changed to 5000 $MEDALS. Name shows instead of class for renamed heroes                                            |
| - Dungeons: Error prompts displays proper message now- Won't be forced to refresh for some errors.                                 |
| - Summoning: Updated text to be clearer, removed excessive text.                                                                   |
| - Claim/Deposit: Invalid input gives an error immediately.                                                                         |
| - Renaming: Error prompt for insufficient balance before signing                                                                   |
| - Upgrading: Error prompt before signing (insufficient exp, medals, ellerium)                                                      |
| - Assignments: 'Second Since Started' changed to 'Time Since Claimed'                                                              |
| - Assignments: time now displays in DD HH MM SS                                                                                    |
| - Binding/Release: Messages now clearer.                                                                                           |
| - Summoning: Messages now clearer.                                                                                                 |
| - Inventory: Added tip to say just re-open the tab instead of refreshing.                                                          |
| - Claim/deposit: Messages now clearer.                                                                                             |
| - Assignments: Messages now clearer.                                                                                               |

| 16 March 2022 - v1.0.3                                     |
| ---------------------------------------------------------- |
| - Assignment rewards from multiple heroes now accumulate   |
| - Claim tax formula has been implemented, currently at 50% |
