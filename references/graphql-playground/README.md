---
description: To query/validate off-chain data.
---

# GraphQL Documentation

The team is committed to the development of a fair, legitimate, and proper product.\
\
With pure on-chain games having the blockchain to act as a ledger for all transactions, we are working on making all off-chain transactions and in-game information accessible as well.

All APIs are currently available without an authentication token. Please do not abuse the APIs or we might need to implement stricter rate limiting or new methods of authentication!

### GraphQL

GraphQL is a query language that lets anyone access our in-game data through available schemas.  Schemas might be updated over time, here is a third-party tool that allows users to automatically view and intuitively select information to pull (usage of the tool is at your own risk)-

{% embed url="https://studio.apollographql.com/sandbox/explorer" %}

{% hint style="info" %}
The screenshots below might be inaccurate to what you see due to schema or UI changes. The general workflow should however stay similar!
{% endhint %}

1. Please set the server URL (the URL in the red box) to: `https://wall.talesofelleria.com/graphql`

<figure><img src="../../.gitbook/assets/image (49).png" alt=""><figcaption><p>Set the server URL to: https://wall.talesofelleria.com/graphql</p></figcaption></figure>

2. You should now see a whole list of schemas available for you to query! For example. `heroes` and `heroesConnection` both allow you to query information regarding heroes.

<figure><img src="../../.gitbook/assets/image (43).png" alt=""><figcaption><p>Available Fields</p></figcaption></figure>

3. We can first click on the '+' to create a new workspace. The box under 'Operation' should now be empty. You'll need to add your query variables and fields next.

<figure><img src="../../.gitbook/assets/image (39).png" alt=""><figcaption><p>New Workspace</p></figcaption></figure>

3. To query for heroes, for example, let's click \`heroes(...): \[Hero]!\` to view what kind of inputs and fields are available.&#x20;

<figure><img src="../../.gitbook/assets/image (38).png" alt=""><figcaption><p>Click on heroes(..) to view its schema.</p></figcaption></figure>

4. Assuming we want to query for everything, we can CTRL+CLICK the `(+)` button to add all fields automatically.

<figure><img src="../../.gitbook/assets/image (40).png" alt=""><figcaption><p>Add all fields.</p></figcaption></figure>

5. Once done, click on the blue 'Play' button. You will see data returned to you.

<figure><img src="../../.gitbook/assets/image (34).png" alt=""><figcaption><p>Responses</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (37).png" alt=""><figcaption><p>You might want to show response in table view if you are only viewing it within the browser.</p></figcaption></figure>

6. To filter for only specific entries, you can click on the (+) beside the Arguments list. You will see a json object appear under "Variables", and you will need to fill the variable values in. You can see each argument type beside their names. \
   \
   For example, to see all burnt heroes, we can filter by owner `0x0000000000000000000000000000000000000000`

<figure><img src="../../.gitbook/assets/image (47).png" alt=""><figcaption></figcaption></figure>

{% hint style="warning" %}
The maximum number of rows of data that can be returned is 10,000 for non-paginated APIs.
{% endhint %}

{% hint style="info" %}
Advanced: To sort the responses within the sandbox, you must use the paginated queries (...Connection) and fill in the orderBy/orderDirection fields.&#x20;

For beginners, it might be easier to download or copy/paste the information and sort within excel/sheets instead.
{% endhint %}

If schemas are outdated, please note them down in #devs-and-builders in Discord:&#x20;

{% embed url="https://discord.com/channels/933280677338701826/974759518988812299" %}

{% tabs %}
{% tab title="Mainnet Endpoint URL" %}
[https://wall.talesofelleria.com/graphql](https://wall.talesofelleria.com/graphql)
{% endtab %}

{% tab title="Testnet Endpoint URL" %}
currently unavailable!
{% endtab %}
{% endtabs %}
