# IDs & Mappings

### Assignments

<table><thead><tr><th width="150">ID</th><th>Representation</th></tr></thead><tbody><tr><td>1</td><td>Misc Tasks</td></tr><tr><td>2</td><td>Guard Duty</td></tr><tr><td>3</td><td>Enemy Recon</td></tr><tr><td>4</td><td>Magic Research</td></tr><tr><td>5</td><td>Hunting Game</td></tr><tr><td>6</td><td>Necropolis Exploration</td></tr><tr><td>7</td><td>Overworld Exploration</td></tr><tr><td>8</td><td>Ruins Exploration</td></tr><tr><td>9</td><td>Lair Exploration</td></tr><tr><td>10</td><td>City Patrol</td></tr><tr><td>11</td><td>Secret Mission</td></tr><tr><td>12</td><td>Altar Maintenance</td></tr><tr><td>13</td><td>Ruins Scouting</td></tr></tbody></table>

### Quests

| ID | Representation                   |
| -- | -------------------------------- |
| 1  | Goblin Plains - Adventure Begins |
| 2  | Goblin Plains - Outskirts        |
| 3  | Goblin Plains - Gold Rush        |
| 4  | Goblin Plains - Outpost          |
| 5  | Goblin Plains - Encampment       |
| 6  | Slime Road - Slime Friend        |
| 7  | Slime Road - Slime Foe           |
| 8  | Slime Road - Slime Ambush        |
| 9  | Slime Road - Slime Bandits       |

### Relics

Please query 'relics' from GQL to see updated Relic Ids!

### Crafting Recipes

Please query 'craftingRecipes' from GQL to see updated Crafting Recipes!
