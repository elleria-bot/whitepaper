# Technology & Innovations

_"We hope to serve as a role model for transparency across the GameFi ecosystem."_

With this vision in mind, we have implemented and are innovating a series of frameworks and methodologies that aims to improve the GameFi scene for both developers and users.

## "BRIDGING 2.0 TO 3.0" Game Modules

We are implementing various popular/famous game features from high quality Web 2.0 mobile games as modules in Tales of Elleria, preparing to onboard different interested parties and bridging the gap between Web 2.0 and 3.0 games.

Current modular features include:

* Account/User Profile System (linked to wallet)
* Daily/Weekly Reward System
* Customizable Gachapon/Minting System
* Upgrade System
* Crafting System
* Inventory/Item Consumption System

## Smart Contracts Framework

#### ERC721 Non-Escrow Staking

Initiative started by Lost Samurise : [https://www.erc721nes.org/](https://www.erc721nes.org/)

Tales of Elleria has implemented the NES logic into our bridging system to allow for several benefits:

* Allows for better analytics on holder distribution.
* Allows players full proper on-chain ownership of their NFTs at all times.
* Doesn't break wallet tracking systems (such as collab.land)
* Allows for the usage of standard ERC721 methods to gather data.

#### ERC1155 Collaborative Framework

Tales of Elleria is using a modularized ERC1155 system which will allow for cross-metaverse integrations of different tokens/NFTs/contract logic.

* Modules allow for custom logic from any contracts to be implemented into the native ERC1155.
* Modules minimize the impact of any ownership breaches in integrations.
* Allows players to benefit from owning non-native tokens/assets.
* Boosts ecosystem growth by promoting cross-metaverse interactions.

This is as simple as implementing the following to allow delegate contracts to mint an item (and perform custom logic checks) using resources from different projects:

```
mapping (uint256 => address) private _itemManagerAddress;

function externalMint(address to, uint256 id, uint256 amount) external {
  require(msg.sender == _itemManagerAddress[id], "External Mint Denied");
  
  _mint(to, id, amount, '');
  emit ExternalMint(to, id, amount);
}

event ExternalMint(address to, uint256 id, uint256 amount);
```

The rest of the logic for item interaction then can be handled on the backend.

## Backend Logic

#### EIP712: SignedTypeDatav4 for Gasless Transactions

![](<../../.gitbook/assets/image (2) (1) (1).png>)

Tales of Elleria uses the above interface to authenticate in-game actions, allowing for most of our actions to be gasless. EIP712 allows for readable signed messages whilst being secure.

* Secure: Wallet's private key must be breached to imitate a transaction.
* Readable: Users are able to clearly see what action they are taking before signing a message.
* Allows for the usage of only gasless signed messages to execute transactions.
* Allows game logic to be immersive and heavily customized, increasing depth and complexity.

**Ledger for off-chain data**

![](<../../.gitbook/assets/image (18) (1).png>)

With the implementation of a off-chain server, there are concerns of data validity & realiability. All player actions are logged and available as receipts that can be queried by the public through a graphQL endpoint.

* Public is able to validate the entire game action history for any player.
* All token emissions are logged in receipts and can be audited and verified.
* All quests results and process are logged in receipts and can also be audited.
