---
description: https://locker.talesofelleria.com/
---

# Token Vesting Dapp

## Token Locker & Linear Vesting Utility

We noticed there isn't a vesting dapp that supports Arbitrum, so we created one that's free of charge and simple to use. You can access it from: [https://locker.talesofelleria.com/](https://locker.talesofelleria.com/)

{% hint style="info" %}
Reach out to Ellerian Prince in the 🛠│token-vesting channel in Discord to get approval for your wallet. (sorry, we have to do this to prevent malicious users and spam)
{% endhint %}

<figure><img src="../../.gitbook/assets/image (17) (1).png" alt=""><figcaption><p>Supports any ERC20, on Arbitrum Goerli/One</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (1) (2) (2).png" alt=""><figcaption><p>Vest to individual/multiple recipients.</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (2) (1) (2).png" alt=""><figcaption><p>View graphically each vest's unlock schedule and progression.</p></figcaption></figure>
