# Terms of Service

#### Definition of Terms

The following terms will be used throughout the document in the capacity defined below:

Tales of Elleria (TELL): Refers to all current and future intellectual products of Tales of Elleria.

User: Refers to anyone who utilizes a product or service provided by TELL.

#### Freedom of Speech

Freedom of Speech is not absolute. The only true place you have full Freedom of Speech is in a public place, when it does not conflict with the values or rights of others, and when you are the only speaker. Within an open forum, the rights of others must be observed. Every place has its own rules. It would be rude to walk into a library and begin shouting at the top of your lungs. Freedom of Speech (or Freedom of Expression) must not infringe on the rights of others.

TELL's social media (Discord, Twitter, etc.) are publicly accessible, and these terms of service are a guideline for what how a user should behave. No one enjoys the concept of censorship, but realistically, we need censorship in order to protect ourselves and, more importantly, our players. These terms are here for the benefit of the players.

Players are forbidden to:

* Harrass, witch hunt, or use sexist, racist or derogatory terms.
* Post illegal/sexual/NSFW topics.
* Insult another player or posting false/misleading information about that player.
* Create excessive amounts of content considered as spam.
* Show prejudice or discriminating against others (towards an individual, people, a race/ethnic group, a religion/belief, etc.)
* Hard advertise other products or services without permission from a staff member.
* Impersonate staff/team members.

#### Updates and Improvements

Users can expect updates and improvements to TELL on a regular basis (at the discretion of the team). Updates can sometimes alter certain elements of the properties in question.

You agree that any calculated change is most likely for the betterment of the community and project at large, and that you will not become hostile or unpleasant to deal with if an update occurs which you do not enjoy, or has (in your opinion) a negative impact on your instance of TELL.

#### **Exploitation of Bugs/Game Mechanics**

Users are not to knowingly exploit a flaw or bug in TELL.

If a user discovers an unexpected behavior that can be abused to give themselves or another player an advantage in the game they are to immediately contact the developers to notify them of the exploit and agree to cease re-creating the unexpected behavior in question.

Players who knowingly exploit bugs will have action taken against their accounts and will forfeit any NFTs (ERC20s/ERC721s/ERC1155s) they might have obtained.

There is an ongoing Bug Bounty program:

* For any game-breaking bug/security vulnerability/severe emissions-related bug found and reported, the user will be awarded with **1x Plague Doctor Secret.**
* For any other bugs that affect operations, the user will be awarded with **1x Jester's Tricks.**

Distribution of Bug Bounty is based on a case-by-case basis.

#### **Third-Party Content and Services**

TELL does not support or endorse any third-party tools. You bear full responsibility for the interaction or usage of any third-party tool.

{% hint style="info" %}
Tools refer to any plug-in, software, hardware, browser extension, bot, script, visualization dashboard, or macro
{% endhint %}

Users of TELL are allowed to use tools, as long as they:

1. Do not obtain unfair material advantage (concerning the data of any NFTs, probability, and parameters)
2. Do not hack, cheat, glitch, exploit, or abuse any features.
3. Do not promote, sell, distribute, share, or solicit tools in any form or way.
4. Will bear full responsibility and accept all loss or damage.
5. Agree not to request support due to the usage of tools.

Users who violate these guidelines will have action taken against their accounts and will forfeit any NFTs (ERC20s/ERC721s/ERC1155s) they might have obtained.

#### Volatility and Risks Associated with Cryptocurrencies

You acknowledge the inherent volatility and risks associated with cryptocurrencies and accept sole responsibility for any outcomes, gains, or losses resulting from your investment decisions. Tales of Elleria shall not be held liable or responsible for any losses, regardless of the circumstances.

#### Limitation or Exclusion of Liability

To the maximum extent permitted by law, TELL and its affiliates, officers, directors, employees, agents, and licensors shall not be liable for any direct, indirect, incidental, special, consequential, or punitive damages, including but not limited to, loss of profits, data, use, goodwill, or other intangible losses, resulting from your access to or use of TELL.

#### Indemnification

To the extent permitted by applicable law, you agree to defend, indemnify, and hold harmless TELL, its affiliates, and licensors, and their respective officers, directors, employees, contractors, agents, licensors, and suppliers from and against any claims, liabilities, damages, judgments, awards, losses, costs, expenses, or fees (including reasonable attorneys' fees) resulting from your violation of these Terms or your use of TELL, including but not limited to, any User Contributions or third-party tools.

#### Intellectual Property Rights

All content, features, and functionality of TELL, including but not limited to, graphics, text, images, logos, software, and code, are the property of Tales of Elleria or its licensors and are protected by copyright, trademark, patent, trade secret, and other intellectual property or proprietary rights laws. You are granted a limited, non-exclusive, non-transferable, and revocable license to access and use TELL for your personal, non-commercial use, subject to these Terms. You may not reproduce, distribute, modify, create derivative works of, publicly display, publicly perform, republish, download, store, or transmit any of the material on TELL, except as expressly permitted by these Terms or with prior written consent from Tales of Elleria.

#### Termination

We reserve the right to terminate or suspend your access to and use of TELL, at our sole discretion, at any time and without notice, for any reason, including but not limited to your violation of these Terms. Upon termination, all provisions of these Terms, which by their nature should survive termination, shall survive termination, including but not limited to ownership provisions, warranty disclaimers, indemnity, and limitations of liability.

#### Governing Law and Dispute Resolution

These Terms shall be governed by and construed in accordance with the laws of the jurisdiction in which TELL operates, without regard to its conflict of law provisions. Any disputes arising out of or in connection with these Terms or your use of TELL shall be resolved through arbitration, in accordance with the rules of the applicable arbitration body. The place of arbitration shall be determined by TELL. The language of arbitration shall be English. The decision of the arbitrator(s) shall be final and binding on both parties.

#### Severability

If any provision of these Terms is held to be invalid, illegal, or unenforceable by a court of competent jurisdiction, the remaining provisions of these Terms shall remain in full force and effect. In such cases, the parties agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions of the Terms shall remain in full force and effect.

#### Entire Agreement

These Terms constitute the entire agreement between you and TELL regarding your use of and access to TELL, and supersede all prior and contemporaneous understandings, agreements, representations, and warranties, both written and oral, regarding TELL.

#### Contact Information

If you have any questions or concerns about these Terms or your use of TELL, please contact us at **talesofelleria@gmail.com** or create a ticket in our discord.

By using or accessing TELL, you acknowledge that you have read, understood, and agree to be bound by these Terms of Service. **If you do not agree to these Terms, please do not use or access TELL.**
