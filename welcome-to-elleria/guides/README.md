---
description: Welcome to Elleria!
---

# Guides

\
We have written up 3 comprehensive guides to easily onboard new players to understand the complex game that Tales of Elleria is. Hope you'll learn more about us from these guides!

<figure><img src="../../.gitbook/assets/image (25).png" alt=""><figcaption></figcaption></figure>

1.  **Visual Guide**\
    For a full visual guide on how to complete the core gameplay loop:

    [Refer to this link ](https://drive.google.com/file/d/1jbJP2x5ElYwzw\_HaDP9mzsggEzadK78N/view)

{% hint style="info" %}
Please note that some of the images in this guide are outdated and from previous versions.
{% endhint %}

2. **General Game Guide**\
   The General Game Guide highlights the most basic gameplay loop within Elleria.

{% content-ref url="general-game-guide.md" %}
[general-game-guide.md](general-game-guide.md)
{% endcontent-ref %}

3. **Advanced Guide**\
   The advanced guide goes beyond the basics, to cover intricate loops that players might be interested to explore.

{% content-ref url="advanced-guide.md" %}
[advanced-guide.md](advanced-guide.md)
{% endcontent-ref %}
