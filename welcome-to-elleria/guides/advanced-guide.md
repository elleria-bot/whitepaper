# Advanced Guide

## **Understand Attributes**

There are two types of attributes in Tales of Elleria, mainly Base Attributes and Combat Attributes. The combat attributes affect your quests & which heroes are worth more/less depending on their stats distribution.

{% content-ref url="../../gameplay-prologue/heroes/attributes/" %}
[attributes](../../gameplay-prologue/heroes/attributes/)
{% endcontent-ref %}

## **Class Traits and Skills**

Skills are special actions that can be performed on quests, used within the combat system. Different skills are useful in different scenarios, especially within team dungeons when multiple heroes can support each other mutually.

Traits are currently predetermined and might be developed further after the other aspects of Elleria are mature. Traits affect the behaviours of classes on Quests. For example, when defending, different classes will react differently.

Click on the link to find out more.

{% content-ref url="../../gameplay-prologue/heroes/class-skills-traits.md" %}
[class-skills-traits.md](../../gameplay-prologue/heroes/class-skills-traits.md)
{% endcontent-ref %}

## **Relics & Crafting**

Relics can be obtained through various sources in the game. Through merchants, quests, and monsters, you'll be able to obtain monster drops, crafting components, and even artifacts.

You can dismantle/refine/upgrade items through the crafting system and transmute them into powerful scrolls, consumables, equipment, accessories, and more!

{% content-ref url="../../gameplay-prologue/relics-drops-system/" %}
[relics-drops-system](../../gameplay-prologue/relics-drops-system/)
{% endcontent-ref %}

{% content-ref url="../../gameplay-prologue/crafting-system/" %}
[crafting-system](../../gameplay-prologue/crafting-system/)
{% endcontent-ref %}

## Equipment

Equipment can be crafted from the Heroes Quarters or obtained from other sources.

Perfect your Equipment's stats and define your own playstyle in preparation for skills! Look for the best main and secondary stats, enhance them to unleash their full potential, and infuse the materials needed to take them to another level!

{% content-ref url="../../gameplay-prologue/equipment-system/" %}
[equipment-system](../../gameplay-prologue/equipment-system/)
{% endcontent-ref %}

## Self-Help Functions &#x20;

If you encounter certain bugs in the game, for example missing heroes or relics, use the self-help functions located at the top left hand corner of the main menu.  ![](<../../.gitbook/assets/image (10) (1) (1).png>)

## **Jesters & Plague Doctors**

At the beginning of mint in March, the team made a mistake with certain heroes' rarity. These NFTs were placed in the wrong rarity tier, leading to mass confusion in the market. As a result, the team made an effort to create a brand new rarity tier for these bugged heroes to increase their perceived value. The team also airdropped these holders with a special relic as a token of apology.

Today, these bugged heroes only have differences in display on trove, which holds no gameplay advantage over regular heroes.

## **Liquidity Provision**&#x20;

Liquidity Provision in Elleria not only contributes to the token's health and grants you $ELM, but also grants you desirable combat stats boost that can make a huge difference while questing. Click on the link below to find out more about Liquidty Boost.&#x20;

Head to the bank to find out more.

{% content-ref url="../../getting-started/liquidity/usdelm-magic-lp.md" %}
[usdelm-magic-lp.md](../../getting-started/liquidity/usdelm-magic-lp.md)
{% endcontent-ref %}

## **Daily & Weekly Tasks**&#x20;

Tasks are meant to be cleared with a **single hero** without any extra effort, allowing **all players** to clear them as long as they play the game daily. Read more below

{% content-ref url="../../gameplay-prologue/game-features/daily-weekly-tasks.md" %}
[daily-weekly-tasks.md](../../gameplay-prologue/game-features/daily-weekly-tasks.md)
{% endcontent-ref %}

## **Understanding MagicSwap**&#x20;

MagicSwap is the gateway to the cross-game economy, where it allows users to provide liquidity, and trade $ELM/$MAGIC. To purchase $ELM to be used within Tales of Elleria, head to [https://magicswap.lol/](https://magicswap.lol/)
