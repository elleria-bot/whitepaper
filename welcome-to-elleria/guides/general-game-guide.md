# General Game Guide

This guide will introduce the basic questing loop to make it easy for new players to understand.

<figure><img src="../../.gitbook/assets/image (54).png" alt=""><figcaption><p>Simple to play, challenging to conquer!</p></figcaption></figure>

## Generic Game Loop

In Tales of Elleria, the core game loop flows as follows:

1. Obtain a Hero through the game/marketplace.
2. Stake the Hero on [Assignments ](../../gameplay-prologue/game-features/assignments/)to earn $MEDALS (game currency)
3. Send the Hero on [Quests ](../../gameplay-prologue/game-features/questing/)using $MEDALS to earn $ELM (ERC-20) and Relics (NFTs)
4. Use $MEDALS and $ELM to [upgrade ](../../gameplay-prologue/equipment-system/upgrade-costs.md)your Hero and increase his combat prowess.
5. Use [Relics](../../gameplay-prologue/relics-drops-system/) to craft scrolls/potions to power up your Hero.
6. Use Relics to craft [Equipment ](../../gameplay-prologue/equipment-system/)to power up your hero.

Your main goal will be to strengthen your heroes as far as you can to have access to higher-difficulty content, which gives more rewards!&#x20;

Use the rewards to further upgrade your heroes, and the game is on!

## Step 1: Obtaining a Hero

{% embed url="https://trove.treasure.lol/collection/tales-of-elleria" %}
Trade Heroes here!
{% endembed %}

You can obtain a hero through the [marketplace ](https://trove.treasure.lol/collection/tales-of-elleria)or mint a FREE Ethereal Hero at the Summoning Altar.&#x20;

{% hint style="info" %}
Ethereal Heroes earn $MEDALS and Soul Dust, a currency used to purchase in-game items from the [Shop](../../gameplay-prologue/game-features/barkeepers-shop.md).
{% endhint %}

### Buying Considerations

When purchasing heroes, there are TWO main considerations to look for.

1. Hero's $MEDALS earning rate
2. Hero's Questing ability

### $MEDALS Earning Rate

$MEDALS are the bread and butter of adventurers, used to go on quests and level up your heroes. For a higher $MEDALS earning rate, look for heroes who can be staked on **class-specific assignments**. These [Assignments ](../../gameplay-prologue/game-features/assignments/assignments-fixed.md)have main and sub-attribute requirements and can be filtered on the marketplace. &#x20;

<table><thead><tr><th width="252">Assignment Type</th><th>Requirements</th><th>MEDALS per second</th></tr></thead><tbody><tr><td>Basic Assignments </td><td><br>No Requirements<br></td><td>(0.1 / 60) $MEDALS</td></tr><tr><td>Class Specific Assignments</td><td>Level Required: 10<br>Main stat >= 86<br>Sub stat >= 61</td><td>(0.2 / 60) + (Hero's main stat - 85) * (0.05 / 60) MEDALS</td></tr></tbody></table>

If you're looking to earn huge amounts of $MEDALS, filter for high levels and Class Specific heroes!

### Hero's Questing Ability

Heroes are classified into Physical (Warrior, Assassin, Rangers, Berserkers) and Magical (Mage, Machinist, Spiritualist). When looking for a hero to purchase, these are the basic guidelines to follow:

<table><thead><tr><th width="142">Attribute</th><th width="225">Affects</th><th>Viability</th></tr></thead><tbody><tr><td>Strength</td><td>Physical Attack</td><td>Decent = 40+<br>Good = 60+<br>Strong = 80+</td></tr><tr><td>Intelligence</td><td>Magic Attack</td><td>Decent 35+<br>Good = 60+<br>Strong = 75+</td></tr><tr><td>Agility</td><td>Crit Rate</td><td>Decent = 40+<br>Good = 50+<br>Strong = 65+</td></tr><tr><td>Vitality</td><td>Maximum HP</td><td>Decent = 40+<br>Good = 50+<br>Strong = 65+</td></tr><tr><td>Endurance</td><td>Physical Defense</td><td>Decent = 50+</td></tr><tr><td>Will</td><td>Magic Defense</td><td>Decent = 50+</td></tr></tbody></table>

The attributes above are estimates of how well a hero performs in quests. Different classes have different max attribute distributions, which you can see in-game or on the marketplace.

We hope this buying guide assists you in selecting heroes to buy!

If you need additional help, please refer to [Discord](https://discord.gg/xmt7FaEZfY), and we'll gladly assist you.

## **Step 2: Staking**

After purchasing your heroes, you can stake them in assignments in the Adventurer's Guild for $MEDALS. The higher the Hero's level, the higher his emissions multiplier in fixed assignments!

| Level   | Emissions Multiplier |
| ------- | -------------------- |
| 1 - 19  | 1x                   |
| 20 - 29 | 2x                   |
| 30 - 39 | 3x                   |
| 40 - 49 | 6x                   |
| 50 - 59 | 10x                  |
| 60 - 69 | 20x                  |
| 70 - 79 | 40x                  |
| 80 - 89 | 60x                  |
| 90 - 99 | 80x                  |
| 100+    | 100x                 |

Players are strongly advised to claim their pending rewards once a week or be subjected to an[ emissions decay](../../gameplay-prologue/game-features/assignments/).

## Step 3: Questing

Quests are the core of the game as not only is the world's growth and mechanics centered around this, but also your main source of experience, equipment, drops, and relics that will become increasingly important as the game expands.

Each hero has a daily entry limit that resets at 0400 UTC. Please refer to the table below for the number of attempts possible.

| Hero Level | Daily Attempts |
| ---------- | -------------- |
| 1 - 19     | 1 attempt      |
| 20 - 39    | 2 attempts     |
| 40 - 69    | 3 attempts     |
| 70 - 89    | 4 attempts     |
| 90+        | 5 attempts     |

Elleria has three quest regions with different rewards:

* Goblin Plains (Relics/Ellerium Shards)
* Slime Road (Skills-related)
* Ancient Ruins ($ELM/Equipment-related)

<table><thead><tr><th width="297.3333333333333">Quests</th><th width="208">Recommended For:</th><th>Special Mobs</th></tr></thead><tbody><tr><td>Goblin Plains - Adventure Begins<br>Slime Road - Slime Friend<br>Slime Road - Slime Foe<br><strong>Ancient Ruins - Peblet's Path</strong></td><td>Medals</td><td>-</td></tr><tr><td>Goblin Plains - Outskirts<br>Goblin Plains - Outposts<br>Goblin Plains - Encampment</td><td>Ellerium Shards<br>Relics</td><td>Goblin Mage<br>Hopgoblin<br>Goblin Leader</td></tr><tr><td>Slime Road - Slime Ambush<br>Slime Road - Slime Bandits</td><td>Relics<br>Skills (future update)</td><td>Lava Slime<br>Slime King</td></tr><tr><td><strong>Ancient Ruins - Boulden's Border</strong><br><strong>Ancient Ruins - Golem's Grudge</strong></td><td>ELM<br>Relics<br>Equipment</td><td>Bouldy<br>Coalheart<br>Gollux</td></tr><tr><td>Special - Gold Rush <br>(weekends only!)</td><td>Clovers</td><td>Gold Goblin</td></tr></tbody></table>

**Quests cost $MEDALS to enter, and you will meet different monsters giving different rewards in every quest. When you meet a special monster, watch out cause they hit harder but also give more rewards!**

**To accumulate $MEDALS, 'Adventure Begins', 'Slime Friend/Foe', and 'Peblet's Path' are recommended. Make sure the hero you send can clear it 100% of the time!**

**This guide breaks quests down into what they are recommended for, so you can choose based on your playstyle. Happy questing!**\


## **Step 4: Enhancing your Hero**

There are three ways you can make your Hero stronger

1. Scrolling using [relics ](../../gameplay-prologue/relics-drops-system/artifacts-consumables.md)will increase base attributes for both assignments/quests.
2. [Leveling ](../../gameplay-prologue/heroes/#increasing-levels)using $MEDALS/$ELM will increase staking emissions and combat ability.
3. Wearing [Equipment](../../gameplay-prologue/equipment-system/) will increase combat ability.

### Leveling

Once your Heroes have earned enough EXP from quests, they are eligible for upgrades to level up! Leveling up increases combat strength and staking emissions, allowing you to progress faster in-game. Check [here ](../../gameplay-prologue/heroes/#increasing-levels)for the full upgrade table.

<table><thead><tr><th width="136">Level</th><th width="149">Medals /lvl</th><th width="128">Ellerium /lvl</th><th width="150">EXP /lvl</th><th width="150">Failure %</th></tr></thead><tbody><tr><td>1 > 9</td><td>2,000</td><td>0</td><td>0</td><td>0%</td></tr><tr><td>10 > 19</td><td>5,000</td><td>0</td><td>+5</td><td>0%</td></tr><tr><td>20 > 29</td><td>15,000</td><td>0</td><td>+10</td><td>0%</td></tr><tr><td>30 > 39</td><td>45,000</td><td>50</td><td>+30</td><td>10%</td></tr><tr><td>40 > 49</td><td>100,000</td><td>500</td><td>+55</td><td>20%</td></tr><tr><td>50 > 59</td><td>200,000</td><td>1,000</td><td>+100</td><td>25%</td></tr></tbody></table>

New users are able to progress by clearing quests of increasing difficulty to accumulate resources, so don't be overwhelmed! Progress at your own pace and only take on what your heroes can handle.

### Scrolling

Scrolls can be crafted at the Heroes Quarters and are the only way to upgrade your heroes' base attributes!

Scrolls are worth the most in improving your hero's $MEDALS earning capabilities. Players can scroll your ordinary heroes until they become eligible for class-specific assignments for a huge boost in $MEDALS, or simply for stronger combat power.

There are different tiers of scrolls available at different crafting costs, see what you need for your heroes!

| Name                   | Effect                                                                        |
| ---------------------- | ----------------------------------------------------------------------------- |
| Risky Scroll           | <p>1 Random Stat:<br>30% -1<br>30% -2<br>30% -3<br>10% +10</p>                |
| Cursed Scroll          | <p>1 Random Stat:<br>40% +10<br>60% -10</p>                                   |
| Chaos Scroll           | <p>1-3 Random Stat:<br>5% -3<br>15% -2<br>20% -1 to +1<br>15% +2<br>5% +3</p> |
| Blessing Scroll        | <p>1-3 Random Stat:<br>30% +1<br>40% +2<br>30% +3</p>                         |
| Exalted Scroll         | <p>1 Random Stat:<br>40% +2<br>30% +3<br>20% +4<br>10% +5</p>                 |
| Miracle Scroll         | <p>3 Random Stat:<br>10% +1<br>20% +2<br>40% +3<br>20% +4<br>10% +5</p>       |
| Lesser Chaos Scroll    | <p>1 Random Stat:<br>33.3% -1 <br>33.3% +0<br>33.3% +1</p>                    |
| Lesser Positive Scroll | <p>1 Random Stat:<br>100% +1</p>                                              |

Read more about crafting [here](../../gameplay-prologue/crafting-system/).\
You can also find the complete list of crafting recipes [here](../../gameplay-prologue/crafting-system/crafting.md).

{% hint style="success" %}
You get 1 Lesser Positive Scroll (SB) for completing weekly tasks! Don't miss out!
{% endhint %}

### Equipment

{% embed url="https://app.treasure.lol/collection/tales-of-elleria-equipment" %}
Trade Equipment here!
{% endembed %}

[Equipment ](../../gameplay-prologue/equipment-system/)can be crafted from the Heroes Quarters, and have huge potential to upgrade your heroes' combat power when they are enhanced, rolled, and infused properly.

Unlike scrolls, equipment can be transferred across heroes and might be a more enticing option for some players in increasing combat strength.

Remember that your Hero's stats scale with its level, so make sure you upgrade their levels as well to have optimal boosts from equipment and scrolls! Look for flat stat improvements for lower-leveled heroes and % stat improvements for higher-leveled heroes.

### Swap Ellerium ($ELM)

{% embed url="https://magicswap.lol/?input=MAGIC&output=ELM" %}
Swap $ELM here!
{% endembed %}

Need $ELM to kickstart your journey? Swap for $ELM using $MAGIC [here](https://magicswap.lol/?input=MAGIC\&output=ELM)!

## Ending Note

In Tales of Elleria, there is no one specific strategy that is the "correct" path for a player to take. Depending on your commitment level and resources, there are multiple ways to play the game.

We have categorized some of the player types and strategies [here ](../../gameplay-prologue/core-gameplay-loop/player-types.md)to allow players to understand how they might want to be involved in the game.

Take your time, and we hope you'll have a great time exploring the vast world of Elleria.\
\
"Fortuna Eterna" - fortune forever favors the brave [🍀](https://emojipedia.org/four-leaf-clover/)



